OPTS=

TARGETS=trottergpu trottercpu trotterref trotterblock

all: $(TARGETS)

trotterref:
		make OPTS="$(OPTS)" -C reference

trotterblock:
		make OPTS="$(OPTS)" -C block

trottercpu:
		make OPTS="$(OPTS)" -C sse

trottergpu:
		make OPTS="$(OPTS)" -C cuda

.PHONY: clean all

clean:
		rm -f *.o $(TARGETS) .depend
		make -C reference clean
		make -C block clean
		make -C sse clean
		make -C cuda clean
