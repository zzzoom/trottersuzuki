#!/bin/bash
for ((i = 256; i <= 2048; i += 128));
do make clean > /dev/null;
   make OPTS="-DMATRIX_WIDTH=$i -DMATRIX_HEIGHT=$i" trotterref;
   ./trotterref | tee -a results.log

   make OPTS="-DMATRIX_WIDTH=$i -DMATRIX_HEIGHT=$i" trotterblock;
   ./trotterblock | tee -a results.log

   make OPTS="-DMATRIX_WIDTH=$i -DMATRIX_HEIGHT=$i" trottercpu;
   ./trottercpu | tee -a results.log

   make OPTS="-DMATRIX_WIDTH=$i -DMATRIX_HEIGHT=$i" trottergpu > /dev/null;
   ./trottergpu | tee -a results.log
done;

