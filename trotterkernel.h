#ifndef __TROTTERKERNEL_H
#define __TROTTERKERNEL_H

#include <string>

template <typename T>
class ITrotterKernel {
public:
    virtual void run_kernels() = 0;
    virtual void wait_for_completion() = 0;
    virtual void get_sample(size_t dest_stride, size_t x, size_t y, size_t width, size_t height, T * dest_real, T * dest_imag) const = 0;

    virtual bool runs_in_place() const = 0;
    virtual std::string get_name() const = 0;
};

#endif
