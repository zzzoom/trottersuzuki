#include <stdint.h>
#include <sys/time.h>

#include <complex>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <fstream>
#include <vector>

#include "../common.h"
#include "../settings.h"
#include "../trotterkernel.h"

#include "cpukernel.h"


template <typename T>
void trotter() {

    // alloc memory for p and the reference kernel
    T * p_real = new T[MATRIX_WIDTH * MATRIX_HEIGHT];
    T * p_imag = new T[MATRIX_WIDTH * MATRIX_HEIGHT];

    // alloc memory for snapshots
    T * samples_real = new T[SAMPLES * SNAPSHOT_WIDTH * SNAPSHOT_HEIGHT];
    T * samples_imag = new T[SAMPLES * SNAPSHOT_WIDTH * SNAPSHOT_HEIGHT];

    // init p
    init_p(p_real, p_imag, MATRIX_WIDTH, MATRIX_HEIGHT);

    // init kernels
    typedef ITrotterKernel<T> kernel_t;
    typedef typename std::vector<kernel_t *> kernel_vector_t;
    kernel_vector_t kernels;
    kernels.push_back(new CPUKernel<T, MATRIX_WIDTH, MATRIX_HEIGHT>(p_real, p_imag, h_a, h_b));

    for (typename kernel_vector_t::iterator it = kernels.begin(); it != kernels.end(); ++it) {
        kernel_t * kernel = *it;
        struct timeval start, end;
        gettimeofday(&start, NULL);
        for (int i = 0; i < ITERATIONS; i++) {
            if (i % SNAPSHOT_EVERY == 0) {
                // TODO?: use a ring buffer and write to disk on the fly
                kernel->wait_for_completion();
                // kernel->get_sample(&samples[(i/SNAPSHOT_EVERY) * SNAPSHOT_WIDTH * SNAPSHOT_HEIGHT], SNAPSHOT_X, SNAPSHOT_Y, SNAPSHOT_WIDTH, SNAPSHOT_HEIGHT);
            }
            kernel->run_kernels();
        }
        kernel->wait_for_completion();
        gettimeofday(&end, NULL);
        long time = (end.tv_sec - start.tv_sec) * 1000000 + (end.tv_usec - start.tv_usec);
        std::cout << MATRIX_WIDTH << "x" << MATRIX_HEIGHT << " " << kernel->get_name() << " " << MATRIX_WIDTH * MATRIX_HEIGHT << " "<< time << std::endl;
    }

    delete[] p_real;
    delete[] p_imag;
    delete[] samples_real;
    delete[] samples_imag;
}

int main(int argc, char** argv) {
    trotter<PRECISION>();
    return 0;
}
