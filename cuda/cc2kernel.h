#ifndef __CC2KERNEL_H
#define __CC2KERNEL_H

#include <cassert>

#include <cuda.h>
#include <helper_cuda.h>

#include "../trotterkernel.h"


template <typename T, int matrix_width, int matrix_height, int STEPS=1>
class CC2Kernel: public ITrotterKernel<T> {
public:
    CC2Kernel(T *p_real, T *p_imag, T a, T b);
    ~CC2Kernel();
    void run_kernels();
    void run_kernel(int k);
    void wait_for_completion();
    void copy_results();
    void get_sample(size_t dest_stride, size_t x, size_t y, size_t width, size_t height, T * dest_real, T * dest_imag) const;

    bool runs_in_place() const { return false; }
    std::string get_name() const { return "CUDA CC 2.x 8-step kernel"; }


private:
    dim3 numBlocks;
    dim3 threadsPerBlock;

    T a;
    T b;
    T *p_real;
    T *p_imag;
    T *pdev_real[2];
    T *pdev_imag[2];
    int sense;

    // NOTE: NEVER USE ODD NUMBERS FOR BLOCK DIMENSIONS
    // thread block / shared memory block width
    static const int BLOCK_X = 32;
    // shared memory block height
    static const int BLOCK_Y = sizeof(T) == 8 ? 32 : 96;
    // thread block height
    static const int STRIDE_Y = 16;

    // halo sizes on each side
    static const int MARGIN_X = 3;
    static const int MARGIN_Y = 4;
};


template<typename T, int matrix_width, int matrix_height, int BLOCK_WIDTH, int BLOCK_HEIGHT, int MARGIN_X, int MARGIN_Y, int BACKWARDS>
inline __device__ void trotter_vert_pair_flexible_nosync(T a, T b, T &cell_r, T &cell_i, int kx, int ky, int py, T rl[BLOCK_HEIGHT][BLOCK_WIDTH], T im[BLOCK_HEIGHT][BLOCK_WIDTH]) {
    T peer_r;
    T peer_i;

    const int ky_peer = ky + 1 - 2 * BACKWARDS;
    if (py >= BACKWARDS && py < matrix_height - 1 + BACKWARDS && ky >= BACKWARDS && ky < BLOCK_HEIGHT - 1 + BACKWARDS) {
        peer_r = rl[ky_peer][kx];
        peer_i = im[ky_peer][kx];
#ifndef DISABLE_FMA
        rl[ky_peer][kx] = a * peer_r - b * cell_i;
        im[ky_peer][kx] = a * peer_i + b * cell_r;
        cell_r = a * cell_r - b * peer_i;
        cell_i = a * cell_i + b * peer_r;
#else
        // NOTE: disabling FMA has worse precision and performance
        //       use only for exact implementation verification against CPU results
        rl[ky_peer][kx] = __fadd_rn(a * peer_r, - b * cell_i);
        im[ky_peer][kx] = __fadd_rn(a * peer_i, b * cell_r);
        cell_r = __fadd_rn(a * cell_r, - b * peer_i);
        cell_i = __fadd_rn(a * cell_i, b * peer_r);
#endif
    }
}


template<typename T, int matrix_width, int matrix_height, int BLOCK_WIDTH, int BLOCK_HEIGHT, int MARGIN_X, int MARGIN_Y, int BACKWARDS>
inline __device__ void trotter_horz_pair_flexible_nosync(T a, T b, T &cell_r, T &cell_i, int kx, int ky, int px, T rl[BLOCK_HEIGHT][BLOCK_WIDTH], T im[BLOCK_HEIGHT][BLOCK_WIDTH]) {
    T peer_r;
    T peer_i;

    const int kx_peer = kx + 1 - 2 * BACKWARDS;
    if (px >= BACKWARDS && px < matrix_width - 1 + BACKWARDS && kx >= BACKWARDS && kx < BLOCK_WIDTH - 1 + BACKWARDS) {
        peer_r = rl[ky][kx_peer];
        peer_i = im[ky][kx_peer];
#ifndef DISABLE_FMA
        rl[ky][kx_peer] = a * peer_r - b * cell_i;
        im[ky][kx_peer] = a * peer_i + b * cell_r;
        cell_r = a * cell_r - b * peer_i;
        cell_i = a * cell_i + b * peer_r;
#else
        // NOTE: disabling FMA has worse precision and performance
        //       use only for exact implementation verification against CPU results
        rl[ky][kx_peer] = __fadd_rn(a * peer_r, - b * cell_i);
        im[ky][kx_peer] = __fadd_rn(a * peer_i, b * cell_r);
        cell_r = __fadd_rn(a * cell_r, - b * peer_i);
        cell_i = __fadd_rn(a * cell_i, b * peer_r);
#endif
    }
}


template<typename T, int matrix_width, int matrix_height, int STEPS, int BLOCK_X, int BLOCK_Y, int MARGIN_X, int MARGIN_Y, int STRIDE_Y>
__launch_bounds__(BLOCK_X * STRIDE_Y)
__global__ void cc2kernel(T a, T b, const T * __restrict__ p_real, const T * __restrict__ p_imag, T * __restrict__ p2_real, T * __restrict__ p2_imag) {
    __shared__ T rl[BLOCK_Y][BLOCK_X];
    __shared__ T im[BLOCK_Y][BLOCK_X];

    int px = blockIdx.x * (BLOCK_X - 2 * STEPS * MARGIN_X) + threadIdx.x - STEPS * MARGIN_X;
    int py = blockIdx.y * (BLOCK_Y - 2 * STEPS * MARGIN_Y) + threadIdx.y - STEPS * MARGIN_Y;

    // Read block from global into shared memory
    if (px >= 0 && px < matrix_width) {
#pragma unroll
        for (int i = 0, pidx = py * matrix_width + px; i < BLOCK_Y / STRIDE_Y; ++i, pidx += STRIDE_Y * matrix_width) {
            if (py + i * STRIDE_Y >= 0 && py + i * STRIDE_Y < matrix_height) {
                rl[threadIdx.y + i * STRIDE_Y][threadIdx.x] = p_real[pidx];
                im[threadIdx.y + i * STRIDE_Y][threadIdx.x] = p_imag[pidx];
            }
        }
    }

    __syncthreads();

    // Place threads along the black cells of a checkerboard pattern
    int sx = threadIdx.x;
    int sy;
    if ((STEPS * MARGIN_X) % 2 == (STEPS * MARGIN_Y) % 2) {
        sy = 2 * threadIdx.y + threadIdx.x % 2;
    } else {
        sy = 2 * threadIdx.y + 1 - threadIdx.x % 2;
    }

    // global y coordinate of the thread on the checkerboard (px remains the same)
    // used for range checks
    int checkerboard_py = blockIdx.y * (BLOCK_Y - 2 * STEPS * MARGIN_Y) + sy - STEPS * MARGIN_Y;

    // Keep the fixed black cells on registers, reds are updated in shared memory
    T cell_r[BLOCK_Y / (STRIDE_Y * 2)];
    T cell_i[BLOCK_Y / (STRIDE_Y * 2)];

#pragma unroll
    // Read black cells to registers
    for (int part = 0; part < BLOCK_Y / (STRIDE_Y * 2); ++part) {
        cell_r[part] = rl[sy + part * 2 * STRIDE_Y][sx];
        cell_i[part] = im[sy + part * 2 * STRIDE_Y][sx];
    }

    // Update cells STEPS full steps
#pragma unroll
    for (int i = 0; i < STEPS; i++) {
        // 12344321
#pragma unroll
        for (int part = 0; part < BLOCK_Y / (STRIDE_Y * 2); ++part) {
            trotter_vert_pair_flexible_nosync<T, matrix_width, matrix_height, BLOCK_X, BLOCK_Y, STEPS * MARGIN_X, STEPS * MARGIN_Y, 0>(a, b, cell_r[part], cell_i[part], sx, sy + part * 2 * STRIDE_Y, checkerboard_py + part * 2 * STRIDE_Y, rl, im);
        }
        __syncthreads();
#pragma unroll
        for (int part = 0; part < BLOCK_Y / (STRIDE_Y * 2); ++part) {
            trotter_horz_pair_flexible_nosync<T, matrix_width, matrix_height, BLOCK_X, BLOCK_Y, STEPS * MARGIN_X, STEPS * MARGIN_Y, 0>(a, b, cell_r[part], cell_i[part], sx, sy + part * 2 * STRIDE_Y, px, rl, im);
        }
        __syncthreads();
#pragma unroll
        for (int part = 0; part < BLOCK_Y / (STRIDE_Y * 2); ++part) {
            trotter_vert_pair_flexible_nosync<T, matrix_width, matrix_height, BLOCK_X, BLOCK_Y, STEPS * MARGIN_X, STEPS * MARGIN_Y, 1>(a, b, cell_r[part], cell_i[part], sx, sy + part * 2 * STRIDE_Y, checkerboard_py + part * 2 * STRIDE_Y, rl, im);
        }
        __syncthreads();
#pragma unroll
        for (int part = 0; part < BLOCK_Y / (STRIDE_Y * 2); ++part) {
            trotter_horz_pair_flexible_nosync<T, matrix_width, matrix_height, BLOCK_X, BLOCK_Y, STEPS * MARGIN_X, STEPS * MARGIN_Y, 1>(a, b, cell_r[part], cell_i[part], sx, sy + part * 2 * STRIDE_Y, px, rl, im);
        }
        __syncthreads();

#pragma unroll
        for (int part = 0; part < BLOCK_Y / (STRIDE_Y * 2); ++part) {
            trotter_horz_pair_flexible_nosync<T, matrix_width, matrix_height, BLOCK_X, BLOCK_Y, STEPS * MARGIN_X, STEPS * MARGIN_Y, 1>(a, b, cell_r[part], cell_i[part], sx, sy + part * 2 * STRIDE_Y, px, rl, im);
        }
        __syncthreads();
#pragma unroll
        for (int part = 0; part < BLOCK_Y / (STRIDE_Y * 2); ++part) {
            trotter_vert_pair_flexible_nosync<T, matrix_width, matrix_height, BLOCK_X, BLOCK_Y, STEPS * MARGIN_X, STEPS * MARGIN_Y, 1>(a, b, cell_r[part], cell_i[part], sx, sy + part * 2 * STRIDE_Y, checkerboard_py + part * 2 * STRIDE_Y, rl, im);
        }
        __syncthreads();
#pragma unroll
        for (int part = 0; part < BLOCK_Y / (STRIDE_Y * 2); ++part) {
            trotter_horz_pair_flexible_nosync<T, matrix_width, matrix_height, BLOCK_X, BLOCK_Y, STEPS * MARGIN_X, STEPS * MARGIN_Y, 0>(a, b, cell_r[part], cell_i[part], sx, sy + part * 2 * STRIDE_Y, px, rl, im);
        }
        __syncthreads();
#pragma unroll
        for (int part = 0; part < BLOCK_Y / (STRIDE_Y * 2); ++part) {
            trotter_vert_pair_flexible_nosync<T, matrix_width, matrix_height, BLOCK_X, BLOCK_Y, STEPS * MARGIN_X, STEPS * MARGIN_Y, 0>(a, b, cell_r[part], cell_i[part], sx, sy + part * 2 * STRIDE_Y, checkerboard_py + part * 2 * STRIDE_Y, rl, im);
        }
        __syncthreads();
    }

    // Write black cells in registers to shared memory
#pragma unroll
    for (int part = 0; part < BLOCK_Y / (STRIDE_Y * 2); ++part) {
        rl[sy + part * 2 * STRIDE_Y][sx] = cell_r[part];
        im[sy + part * 2 * STRIDE_Y][sx] = cell_i[part];
    }
    __syncthreads();

    // discard the halo and copy results from shared to global memory
    sx = threadIdx.x + STEPS * MARGIN_X;
    sy = threadIdx.y + STEPS * MARGIN_Y;
    px += STEPS * MARGIN_X;
    py += STEPS * MARGIN_Y;
    if (sx < BLOCK_X - STEPS * MARGIN_X && px < matrix_width) {
#pragma unroll
        for (int i = 0, pidx = py * matrix_width + px; i < BLOCK_Y / STRIDE_Y; ++i, pidx += STRIDE_Y * matrix_width) {
            if (sy + i * STRIDE_Y < BLOCK_Y - STEPS * MARGIN_Y && py + i * STRIDE_Y < matrix_height) {
                p2_real[pidx] = rl[sy + i * STRIDE_Y][sx];
                p2_imag[pidx] = im[sy + i * STRIDE_Y][sx];
            }
        }
    }
}


template <typename T, int matrix_width, int matrix_height, int STEPS>
CC2Kernel<T, matrix_width, matrix_height, STEPS>::CC2Kernel(T *_p_real, T *_p_imag, T _a, T _b):
    p_real(_p_real),
    p_imag(_p_imag),
    threadsPerBlock(BLOCK_X, STRIDE_Y),
    numBlocks((matrix_width  + (BLOCK_X - 2 * STEPS * MARGIN_X) - 1) / (BLOCK_X - 2 * STEPS * MARGIN_X),
              (matrix_height + (BLOCK_Y - 2 * STEPS * MARGIN_Y) - 1) / (BLOCK_Y - 2 * STEPS * MARGIN_Y)),
    sense(0),
    a(_a),
    b(_b)
{
    checkCudaErrors(cudaMalloc(reinterpret_cast<void**>(&pdev_real[0]), matrix_width * matrix_height * sizeof(T)));
    checkCudaErrors(cudaMalloc(reinterpret_cast<void**>(&pdev_real[1]), matrix_width * matrix_height * sizeof(T)));
    checkCudaErrors(cudaMalloc(reinterpret_cast<void**>(&pdev_imag[0]), matrix_width * matrix_height * sizeof(T)));
    checkCudaErrors(cudaMalloc(reinterpret_cast<void**>(&pdev_imag[1]), matrix_width * matrix_height * sizeof(T)));
    checkCudaErrors(cudaMemcpy(pdev_real[0], p_real, matrix_width * matrix_height * sizeof(T), cudaMemcpyHostToDevice));
    checkCudaErrors(cudaMemcpy(pdev_imag[0], p_imag, matrix_width * matrix_height * sizeof(T), cudaMemcpyHostToDevice));
}


template <typename T, int matrix_width, int matrix_height, int STEPS>
CC2Kernel<T, matrix_width, matrix_height, STEPS>::~CC2Kernel() {
    checkCudaErrors(cudaFree(pdev_real[0]));
    checkCudaErrors(cudaFree(pdev_real[1]));
    checkCudaErrors(cudaFree(pdev_imag[0]));
    checkCudaErrors(cudaFree(pdev_imag[1]));
}

template <typename T, int matrix_width, int matrix_height, int STEPS>
void CC2Kernel<T, matrix_width, matrix_height, STEPS>::run_kernels() {
    run_kernel(1);
}

template <typename T, int matrix_width, int matrix_height, int STEPS>
void CC2Kernel<T, matrix_width, matrix_height, STEPS>::run_kernel(int k) {
    switch (k) {
    case 1:
        cc2kernel<T, matrix_width, matrix_height, STEPS, BLOCK_X, BLOCK_Y, MARGIN_X, MARGIN_Y, STRIDE_Y><<<numBlocks, threadsPerBlock>>>(a, b, pdev_real[sense], pdev_imag[sense], pdev_real[1-sense], pdev_imag[1-sense]);
        break;
    default:
        // TODO: throw
        break;
    }
    sense = 1 - sense;
    getLastCudaError("Kernel error in CC2Kernel::run_kernel");
}

template <typename T, int matrix_width, int matrix_height, int STEPS>
void CC2Kernel<T, matrix_width, matrix_height, STEPS>::wait_for_completion() {
    checkCudaErrors(cudaThreadSynchronize());
}

template <typename T, int matrix_width, int matrix_height, int STEPS>
void CC2Kernel<T, matrix_width, matrix_height, STEPS>::copy_results() {
    checkCudaErrors(cudaMemcpy(p_real, pdev_real[sense], matrix_width * matrix_height * sizeof(T), cudaMemcpyDeviceToHost));
    checkCudaErrors(cudaMemcpy(p_imag, pdev_imag[sense], matrix_width * matrix_height * sizeof(T), cudaMemcpyDeviceToHost));
}

template <typename T, int matrix_width, int matrix_height, int STEPS>
void CC2Kernel<T, matrix_width, matrix_height, STEPS>::get_sample(size_t dest_stride, size_t x, size_t y, size_t width, size_t height, T * dest_real, T * dest_imag) const {
    assert(x < matrix_width);
    assert(y < matrix_height);
    assert(x + width <= matrix_width);
    assert(y + height <= matrix_height);
    checkCudaErrors(cudaMemcpy2D(dest_real, dest_stride * sizeof(T), &(pdev_real[sense][y * matrix_width + x]), matrix_width * sizeof(T), width * sizeof(T), height, cudaMemcpyDeviceToHost));
    checkCudaErrors(cudaMemcpy2D(dest_imag, dest_stride * sizeof(T), &(pdev_imag[sense][y * matrix_width + x]), matrix_width * sizeof(T), width * sizeof(T), height, cudaMemcpyDeviceToHost));
}

#endif
