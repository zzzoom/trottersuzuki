#include <stdint.h>
#include <sys/time.h>

#include <algorithm>
#include <complex>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <fstream>
#include <vector>

#include "../common.h"
#include "../settings.h"
#include "../trotterkernel.h"

// #include "cpublock.h"
#include "cpublocksse.h"

#ifdef COMPARE_CPU
#include "../reference/cpukernel.h"
#endif

template <typename T>
void trotter() {

    // alloc memory for p and the reference kernel
    T * p_real = new T[MATRIX_WIDTH * MATRIX_HEIGHT];
    T * p_imag = new T[MATRIX_WIDTH * MATRIX_HEIGHT];

    // alloc memory for snapshots
    T * samples_real = new T[SAMPLES * SNAPSHOT_WIDTH * SNAPSHOT_HEIGHT];
    T * samples_imag = new T[SAMPLES * SNAPSHOT_WIDTH * SNAPSHOT_HEIGHT];

    // init p
    init_p(p_real, p_imag, MATRIX_WIDTH, MATRIX_HEIGHT);

#ifdef COMPARE_CPU
    T * ref_real = new T[MATRIX_WIDTH * MATRIX_HEIGHT];
    T * ref_imag = new T[MATRIX_WIDTH * MATRIX_HEIGHT];
    std::copy(p_real, p_real + (MATRIX_WIDTH * MATRIX_HEIGHT), ref_real);
    std::copy(p_imag, p_imag + (MATRIX_WIDTH * MATRIX_HEIGHT), ref_imag);
#endif

    ITrotterKernel<T> * kernel = new CPUBlockSSEKernel<T, MATRIX_WIDTH, MATRIX_HEIGHT>(p_real, p_imag, h_a, h_b);
//    ITrotterKernel<T> * kernel = new CPUBlock<T, MATRIX_WIDTH, MATRIX_HEIGHT>(p_real, p_imag, h_a, h_b);

    struct timeval start, end;
    gettimeofday(&start, NULL);

    for (int i = 0; i < ITERATIONS; i++) {
        if (i % SNAPSHOT_EVERY == 0) {
            // TODO?: use a ring buffer and write to disk on the fly
            kernel->wait_for_completion();
            // kernel->get_sample(&samples[(i/SNAPSHOT_EVERY) * SNAPSHOT_WIDTH * SNAPSHOT_HEIGHT], SNAPSHOT_X, SNAPSHOT_Y, SNAPSHOT_WIDTH, SNAPSHOT_HEIGHT);
        }
        kernel->run_kernels();
    }
    kernel->wait_for_completion();

    gettimeofday(&end, NULL);
    long time = (end.tv_sec - start.tv_sec) * 1000000 + (end.tv_usec - start.tv_usec);
    std::cout << MATRIX_WIDTH << "x" << MATRIX_HEIGHT << " " << kernel->get_name() << " " << MATRIX_WIDTH * MATRIX_HEIGHT << " "<< time << std::endl;

#ifdef COMPARE_CPU
    kernel->get_sample(MATRIX_WIDTH, 0, 0, MATRIX_WIDTH, MATRIX_HEIGHT, p_real, p_imag);

    ITrotterKernel<T> * ref_kernel = new CPUKernel<T, MATRIX_WIDTH, MATRIX_HEIGHT>(ref_real, ref_imag, h_a, h_b);
    for (unsigned int i = 0; i < ITERATIONS; i++) {
        ref_kernel->run_kernels();
    }
    ref_kernel->wait_for_completion();

    if (!ref_kernel->runs_in_place()) {
        ref_kernel->get_sample(MATRIX_WIDTH, 0, 0, MATRIX_WIDTH, MATRIX_HEIGHT, ref_real, ref_imag);
    }
    measure_error<T>(ref_real, ref_imag, p_real, p_imag, MATRIX_WIDTH, MATRIX_WIDTH, MATRIX_HEIGHT);

    delete[] ref_real;
    delete[] ref_imag;
    delete ref_kernel;
#endif

    delete[] p_real;
    delete[] p_imag;
    delete[] samples_real;
    delete[] samples_imag;
    delete kernel;
}

int main(int argc, char** argv) {
    trotter<PRECISION>();
    return 0;
}
