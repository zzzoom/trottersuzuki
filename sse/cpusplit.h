#ifndef __CPUSPLIT_H
#define __CPUSPLIT_H

#include <sstream>
#include <cassert>
#include <cstdlib>
#include <xmmintrin.h>
#include <emmintrin.h>

#ifdef _OPENMP
#include <omp.h>
#endif

#include "../trotterkernel.h"

template <typename T, int matrix_width, int matrix_height>
class CPUSplitKernel: public ITrotterKernel<T> {
public:
    CPUSplitKernel(T *p_real, T *p_imag, T _a, T _b);
    ~CPUSplitKernel();
    void run_kernels();
    void run_kernel(int k);
    void wait_for_completion();
    void copy_results();
    void get_sample(size_t dest_stride, size_t x, size_t y, size_t width, size_t height, T * dest_real, T * dest_imag) const;

    bool runs_in_place() const { return false; }
    std::string get_name() const {
#ifdef _OPENMP
        std::stringstream name;
        name << "OpenMP SSE split data kernel (" << omp_get_max_threads() << " threads)";
        return name.str();
#else
        return "CPU SSE split data kernel";
#endif
    };


private:
    T *p_real;
    T *p_imag;
    T *r00, *r01, *r10, *r11;
    T *i00, *i01, *i10, *i11;
    T a;
    T b;
};


/************************************
* Unoptimized quadrant split kernel *
************************************/
template <typename T, int matrix_width, int matrix_height, int offset_x, int offset_y>
static inline void update(T a, T b, T * __restrict__ r1, T * __restrict__ i1, T * __restrict__ r2, T * __restrict__ i2) {
    T next_r1, next_r2, next_i1, next_i2;
#ifdef _OPENMP
#pragma omp parallel for default(shared) private(next_r1, next_r2, next_i1, next_i2)
#endif
    for (int i = 0; i < matrix_height/2 - offset_y; i++) {
        int idx1 = i * (matrix_width/2);
        int idx2 = (i+offset_y) * (matrix_width/2) + offset_x;
        for (int j = 0; j < matrix_width/2 - offset_x; j++, idx1++, idx2++) {
            next_r1 = a * r1[idx1] - b * i2[idx2];
            next_i1 = a * i1[idx1] + b * r2[idx2];
            next_r2 = a * r2[idx2] - b * i1[idx1];
            next_i2 = a * i2[idx2] + b * r1[idx1];
            r1[idx1] = next_r1;
            i1[idx1] = next_i1;
            r2[idx2] = next_r2;
            i2[idx2] = next_i2;
        }
    }
}


/***************
* SSE variants *
***************/
template <int matrix_width, int matrix_height, int offset_y>
static inline void update_shifty_sse(float a, float b, float * __restrict__ r1, float * __restrict__ i1, float * __restrict__ r2, float * __restrict__ i2) {
#ifdef _OPENMP
#pragma omp parallel default(shared)
{
#endif
    __m128 aq, bq;
    aq = _mm_load1_ps(&a);
    bq = _mm_load1_ps(&b);
#ifdef _OPENMP
#pragma omp for
#endif
    for (int i = 0; i < matrix_height/2 - offset_y; i++) {
        int idx1 = i * (matrix_width/2);
        int idx2 = (i+offset_y) * (matrix_width/2);
        int j = 0;
        for (; j < matrix_width/2 - (matrix_width/2) % 4; j += 4, idx1 += 4, idx2 += 4) {
            __m128 r1q = _mm_load_ps(&r1[idx1]);
            __m128 i1q = _mm_load_ps(&i1[idx1]);
            __m128 r2q = _mm_load_ps(&r2[idx2]);
            __m128 i2q = _mm_load_ps(&i2[idx2]);
            __m128 next_r1q = _mm_sub_ps(_mm_mul_ps(r1q, aq), _mm_mul_ps(i2q, bq));
            __m128 next_i1q = _mm_add_ps(_mm_mul_ps(i1q, aq), _mm_mul_ps(r2q, bq));
            __m128 next_r2q = _mm_sub_ps(_mm_mul_ps(r2q, aq), _mm_mul_ps(i1q, bq));
            __m128 next_i2q = _mm_add_ps(_mm_mul_ps(i2q, aq), _mm_mul_ps(r1q, bq));
            _mm_store_ps(&r1[idx1], next_r1q);
            _mm_store_ps(&i1[idx1], next_i1q);
            _mm_store_ps(&r2[idx2], next_r2q);
            _mm_store_ps(&i2[idx2], next_i2q);
        }
        for (; j < matrix_width / 2; ++j, ++idx1, ++idx2) {
            float next_r1 = a * r1[idx1] - b * i2[idx2];
            float next_i1 = a * i1[idx1] + b * r2[idx2];
            float next_r2 = a * r2[idx2] - b * i1[idx1];
            float next_i2 = a * i2[idx2] + b * r1[idx1];
            r1[idx1] = next_r1;
            i1[idx1] = next_i1;
            r2[idx2] = next_r2;
            i2[idx2] = next_i2;
        }
    }
#ifdef _OPENMP
}
#endif
}


template <int matrix_width, int matrix_height, int offset_y>
static inline void update_shifty_sse(double a, double b, double * __restrict__ r1, double * __restrict__ i1, double * __restrict__ r2, double * __restrict__ i2) {
#ifdef _OPENMP
#pragma omp parallel default(shared)
{
#endif
    __m128d ap, bp;
    ap = _mm_load1_pd(&a);
    bp = _mm_load1_pd(&b);
#ifdef _OPENMP
#pragma omp for
#endif
    for (int i = 0; i < matrix_height/2 - offset_y; i++) {
        int idx1 = i * (matrix_width/2);
        int idx2 = (i+offset_y) * (matrix_width/2);
        int j = 0;
        for (; j < matrix_width/2 - (matrix_width/2) % 2; j += 2, idx1 += 2, idx2 += 2) {
            __m128d r1p = _mm_load_pd(&r1[idx1]);
            __m128d i1p = _mm_load_pd(&i1[idx1]);
            __m128d r2p = _mm_load_pd(&r2[idx2]);
            __m128d i2p = _mm_load_pd(&i2[idx2]);
            __m128d next_r1p = _mm_sub_pd(_mm_mul_pd(r1p, ap), _mm_mul_pd(i2p, bp));
            __m128d next_i1p = _mm_add_pd(_mm_mul_pd(i1p, ap), _mm_mul_pd(r2p, bp));
            __m128d next_r2p = _mm_sub_pd(_mm_mul_pd(r2p, ap), _mm_mul_pd(i1p, bp));
            __m128d next_i2p = _mm_add_pd(_mm_mul_pd(i2p, ap), _mm_mul_pd(r1p, bp));
            _mm_store_pd(&r1[idx1], next_r1p);
            _mm_store_pd(&i1[idx1], next_i1p);
            _mm_store_pd(&r2[idx2], next_r2p);
            _mm_store_pd(&i2[idx2], next_i2p);
        }
        for (; j < matrix_width / 2; ++j, ++idx1, ++idx2) {
            double next_r1 = a * r1[idx1] - b * i2[idx2];
            double next_i1 = a * i1[idx1] + b * r2[idx2];
            double next_r2 = a * r2[idx2] - b * i1[idx1];
            double next_i2 = a * i2[idx2] + b * r1[idx1];
            r1[idx1] = next_r1;
            i1[idx1] = next_i1;
            r2[idx2] = next_r2;
            i2[idx2] = next_i2;
        }
    }
#ifdef _OPENMP
}
#endif
}


template <int matrix_width, int matrix_height, int offset_x>
static inline void update_shiftx_sse(float a, float b, float * __restrict__ r1, float * __restrict__ i1, float * __restrict__ r2, float * __restrict__ i2) {
#ifdef _OPENMP
#pragma omp parallel default(shared)
{
#endif
    __m128 aq, bq;
    aq = _mm_load1_ps(&a);
    bq = _mm_load1_ps(&b);
#ifdef _OPENMP
#pragma omp for
#endif
    for (int i = 0; i < matrix_height/2; i++) {
        int idx1 = i * (matrix_width/2);
        int idx2 = i * (matrix_width/2) + offset_x;
        int j = 0;
        for (; j < (matrix_width/2 - offset_x) - (matrix_width/2 - offset_x) % 4; j += 4, idx1 += 4, idx2 += 4) {
            __m128 r1q = _mm_load_ps(&r1[idx1]);
            __m128 i1q = _mm_load_ps(&i1[idx1]);
            __m128 r2q = _mm_loadu_ps(&r2[idx2]);
            __m128 i2q = _mm_loadu_ps(&i2[idx2]);
            __m128 next_r1q = _mm_sub_ps(_mm_mul_ps(r1q, aq), _mm_mul_ps(i2q, bq));
            __m128 next_i1q = _mm_add_ps(_mm_mul_ps(i1q, aq), _mm_mul_ps(r2q, bq));
            __m128 next_r2q = _mm_sub_ps(_mm_mul_ps(r2q, aq), _mm_mul_ps(i1q, bq));
            __m128 next_i2q = _mm_add_ps(_mm_mul_ps(i2q, aq), _mm_mul_ps(r1q, bq));
            _mm_store_ps(&r1[idx1], next_r1q);
            _mm_store_ps(&i1[idx1], next_i1q);
            _mm_storeu_ps(&r2[idx2], next_r2q);
            _mm_storeu_ps(&i2[idx2], next_i2q);
        }
        for (; j < matrix_width/2 - offset_x; j++, idx1++, idx2++) {
            float next_r1 = a * r1[idx1] - b * i2[idx2];
            float next_i1 = a * i1[idx1] + b * r2[idx2];
            float next_r2 = a * r2[idx2] - b * i1[idx1];
            float next_i2 = a * i2[idx2] + b * r1[idx1];
            r1[idx1] = next_r1;
            i1[idx1] = next_i1;
            r2[idx2] = next_r2;
            i2[idx2] = next_i2;
        }
    }
#ifdef _OPENMP
}
#endif
}


template <int matrix_width, int matrix_height, int offset_x>
static inline void update_shiftx_sse(double a, double b, double * __restrict__ r1, double * __restrict__ i1, double * __restrict__ r2, double * __restrict__ i2) {
#ifdef _OPENMP
#pragma omp parallel default(shared)
{
#endif
    __m128d ap, bp;
    ap = _mm_load1_pd(&a);
    bp = _mm_load1_pd(&b);
#ifdef _OPENMP
#pragma omp for
#endif
    for (int i = 0; i < matrix_height/2; i++) {
        int idx1 = i * (matrix_width/2);
        int idx2 = i * (matrix_width/2) + offset_x;
        int j = 0;
        for (; j < (matrix_width/2 - offset_x) - (matrix_width/2 - offset_x) % 2; j += 2, idx1 += 2, idx2 += 2) {
            __m128d r1p = _mm_load_pd(&r1[idx1]);
            __m128d i1p = _mm_load_pd(&i1[idx1]);
            __m128d r2p = _mm_loadu_pd(&r2[idx2]);
            __m128d i2p = _mm_loadu_pd(&i2[idx2]);
            __m128d next_r1p = _mm_sub_pd(_mm_mul_pd(r1p, ap), _mm_mul_pd(i2p, bp));
            __m128d next_i1p = _mm_add_pd(_mm_mul_pd(i1p, ap), _mm_mul_pd(r2p, bp));
            __m128d next_r2p = _mm_sub_pd(_mm_mul_pd(r2p, ap), _mm_mul_pd(i1p, bp));
            __m128d next_i2p = _mm_add_pd(_mm_mul_pd(i2p, ap), _mm_mul_pd(r1p, bp));
            _mm_store_pd(&r1[idx1], next_r1p);
            _mm_store_pd(&i1[idx1], next_i1p);
            _mm_storeu_pd(&r2[idx2], next_r2p);
            _mm_storeu_pd(&i2[idx2], next_i2p);
        }
        for (; j < matrix_width/2 - offset_x; j++, idx1++, idx2++) {
            double next_r1 = a * r1[idx1] - b * i2[idx2];
            double next_i1 = a * i1[idx1] + b * r2[idx2];
            double next_r2 = a * r2[idx2] - b * i1[idx1];
            double next_i2 = a * i2[idx2] + b * r1[idx1];
            r1[idx1] = next_r1;
            i1[idx1] = next_i1;
            r2[idx2] = next_r2;
            i2[idx2] = next_i2;
        }
    }
#ifdef _OPENMP
}
#endif
}


template <typename T, int matrix_width, int matrix_height>
CPUSplitKernel<T, matrix_width,matrix_height>::CPUSplitKernel(T *_p_real, T *_p_imag, T _a, T _b):
    p_real(_p_real),
    p_imag(_p_imag),
    a(_a),
    b(_b)
{
    assert (matrix_width % 2 == 0);
    assert (matrix_height % 2 == 0);

    posix_memalign(reinterpret_cast<void**>(&r00), 64, ((matrix_width * matrix_height) / 4) * sizeof(T));
    posix_memalign(reinterpret_cast<void**>(&r01), 64, ((matrix_width * matrix_height) / 4) * sizeof(T));
    posix_memalign(reinterpret_cast<void**>(&r10), 64, ((matrix_width * matrix_height) / 4) * sizeof(T));
    posix_memalign(reinterpret_cast<void**>(&r11), 64, ((matrix_width * matrix_height) / 4) * sizeof(T));
    posix_memalign(reinterpret_cast<void**>(&i00), 64, ((matrix_width * matrix_height) / 4) * sizeof(T));
    posix_memalign(reinterpret_cast<void**>(&i01), 64, ((matrix_width * matrix_height) / 4) * sizeof(T));
    posix_memalign(reinterpret_cast<void**>(&i10), 64, ((matrix_width * matrix_height) / 4) * sizeof(T));
    posix_memalign(reinterpret_cast<void**>(&i11), 64, ((matrix_width * matrix_height) / 4) * sizeof(T));
    for (int i = 0; i < matrix_height/2; i++) {
        for (int j = 0; j < matrix_width/2; j++) {
            r00[i * matrix_width / 2 + j] = p_real[2 * i * matrix_width + 2 * j];
            i00[i * matrix_width / 2 + j] = p_imag[2 * i * matrix_width + 2 * j];
            r01[i * matrix_width / 2 + j] = p_real[2 * i * matrix_width + 2 * j + 1];
            i01[i * matrix_width / 2 + j] = p_imag[2 * i * matrix_width + 2 * j + 1];
        }
        for (int j = 0; j < matrix_width/2; j++) {
            r10[i * matrix_width / 2 + j] = p_real[(2 * i + 1) * matrix_width + 2 * j];
            i10[i * matrix_width / 2 + j] = p_imag[(2 * i + 1) * matrix_width + 2 * j];
            r11[i * matrix_width / 2 + j] = p_real[(2 * i + 1) * matrix_width + 2 * j + 1];
            i11[i * matrix_width / 2 + j] = p_imag[(2 * i + 1) * matrix_width + 2 * j + 1];
        }
    }
}

template <typename T, int matrix_width, int matrix_height>
CPUSplitKernel<T,matrix_width,matrix_height>::~CPUSplitKernel() {
    free(r00);
    free(r01);
    free(r10);
    free(r11);
    free(i00);
    free(i01);
    free(i10);
    free(i11);
}

template <typename T, int matrix_width, int matrix_height>
void CPUSplitKernel<T,matrix_width,matrix_height>::run_kernels() {
    run_kernel(1);
    run_kernel(2);
    run_kernel(3);
    run_kernel(4);
    run_kernel(4);
    run_kernel(3);
    run_kernel(2);
    run_kernel(1);
}

template <typename T, int matrix_width, int matrix_height>
void CPUSplitKernel<T,matrix_width,matrix_height>::run_kernel(int k) {

    switch (k) {
#ifdef USE_SSE_INTRINSICS
    // NOTE: shiftx and shifty have the same result for offset=0 and shifty is faster
    case 1:
        update_shifty_sse<matrix_width,matrix_height,0u>(a, b, r00, i00, r10, i10);
        update_shifty_sse<matrix_width,matrix_height,1u>(a, b, r11, i11, r01, i01);
        break;
    case 2:
        update_shifty_sse<matrix_width,matrix_height,0u>(a, b, r00, i00, r01, i01);
        update_shiftx_sse<matrix_width,matrix_height,1u>(a, b, r11, i11, r10, i10);
        break;
    case 3:
        update_shifty_sse<matrix_width,matrix_height,0u>(a, b, r01, i01, r11, i11);
        update_shifty_sse<matrix_width,matrix_height,1u>(a, b, r10, i10, r00, i00);
        break;
    case 4:
        update_shifty_sse<matrix_width,matrix_height,0u>(a, b, r10, i10, r11, i11);
        update_shiftx_sse<matrix_width,matrix_height,1u>(a, b, r01, i01, r00, i00);
        break;
#else
    case 1:
        update<T,matrix_width,matrix_height,0u,0u>(a, b, r00, i00, r10, i10);
        update<T,matrix_width,matrix_height,0u,1u>(a, b, r11, i11, r01, i01);
        break;
    case 2:
        update<T,matrix_width,matrix_height,0u,0u>(a, b, r00, i00, r01, i01);
        update<T,matrix_width,matrix_height,1u,0u>(a, b, r11, i11, r10, i10);
        break;
    case 3:
        update<T,matrix_width,matrix_height,0u,0u>(a, b, r01, i01, r11, i11);
        update<T,matrix_width,matrix_height,0u,1u>(a, b, r10, i10, r00, i00);
        break;
    case 4:
        update<T,matrix_width,matrix_height,0u,0u>(a, b, r10, i10, r11, i11);
        update<T,matrix_width,matrix_height,1u,0u>(a, b, r01, i01, r00, i00);
        break;
#endif
    default:
        // TODO: throw
        break;
    }
}

template <typename T, int matrix_width, int matrix_height>
void CPUSplitKernel<T,matrix_width,matrix_height>::wait_for_completion() {
}

template <typename T, int matrix_width, int matrix_height>
void CPUSplitKernel<T,matrix_width,matrix_height>::copy_results() {
    for (int i = 0; i < matrix_height / 2; i++) {
        for (int j = 0; j < matrix_width / 2; j++) {
            p_real[2 * i * matrix_width + 2 * j] = r00[i * matrix_width / 2 + j];
            p_imag[2 * i * matrix_width + 2 * j] = i00[i * matrix_width / 2 + j];
            p_real[2 * i * matrix_width + 2 * j + 1] = r01[i * matrix_width / 2 + j];
            p_imag[2 * i * matrix_width + 2 * j + 1] = i01[i * matrix_width / 2 + j];
        }
        for (int j = 0; j < matrix_width / 2; j++) {
            p_real[(2 * i + 1) * matrix_width + 2 * j] = r10[i * matrix_width / 2 + j];
            p_imag[(2 * i + 1) * matrix_width + 2 * j] = i10[i * matrix_width / 2 + j];
            p_real[(2 * i + 1) * matrix_width + 2 * j + 1] = r11[i * matrix_width / 2 + j];
            p_imag[(2 * i + 1) * matrix_width + 2 * j + 1] = i11[i * matrix_width / 2 + j];
        }
    }
}


template <typename T, int matrix_width, int matrix_height>
void CPUSplitKernel<T, matrix_width,matrix_height>::get_sample(size_t dest_stride, size_t x, size_t y, size_t width, size_t height, T * dest_real, T * dest_imag) const {
    get_quadrant_sample<T>(r00, r01, r10, r11, i00, i01, i10, i11, matrix_width/2, dest_stride, x, y, width, height, dest_real, dest_imag);
}

#endif
