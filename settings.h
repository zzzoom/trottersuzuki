#ifndef _SETTINGS_H_
#define _SETTINGS_H_

#define MAX_FILENAME_SIZE 128

#ifndef MATRIX_WIDTH
#define MATRIX_WIDTH 640
#endif

#ifndef MATRIX_HEIGHT
#define MATRIX_HEIGHT 640
#endif

// TODO: no point having these two defined at compile time
#ifndef ITERATIONS
#define ITERATIONS 1000
#endif

#ifndef SNAPSHOT_EVERY
#define SNAPSHOT_EVERY 1000
#endif

#define SAMPLES (ITERATIONS / SNAPSHOT_EVERY)

// NOTE: 640 x 640 x sizeof(float2) = 3.125MB per snapshot
//       10000/250 = 40 frames = 125MB
// WARNING: PINNED MEMORY ISNT PAGEABLE

#ifndef SNAPSHOT_X
#define SNAPSHOT_X 0
#endif

#ifndef SNAPSHOT_Y
#define SNAPSHOT_Y 0
#endif

#ifndef SNAPSHOT_WIDTH
#define SNAPSHOT_WIDTH (MATRIX_WIDTH)
#endif

#ifndef SNAPSHOT_HEIGHT
#define SNAPSHOT_HEIGHT (MATRIX_HEIGHT)
#endif

#ifndef PRECISION
#define PRECISION float
#endif

// Constant storage
static const double h_a = cos(0.02);
static const double h_b = sin(0.02);

#endif
