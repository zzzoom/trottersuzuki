#ifndef __CPUBLOCKSSE_H
#define __CPUBLOCKSSE_H

#include <iostream>
#include <sstream>
#include <cassert>
#include <cstdlib>
#include <xmmintrin.h>
#include <emmintrin.h>

#ifdef _OPENMP
#include <omp.h>
#endif

#include "../trotterkernel.h"

template <typename T, int matrix_width, int matrix_height>
class CPUBlockSSEKernel: public ITrotterKernel<T> {
public:
    CPUBlockSSEKernel(T *p_real, T *p_imag, T _a, T _b);
    ~CPUBlockSSEKernel();
    void run_kernels();
    void wait_for_completion();
    void get_sample(size_t dest_stride, size_t x, size_t y, size_t width, size_t height, T * dest_real, T * dest_imag) const;

    bool runs_in_place() const { return false; }
    std::string get_name() const {
#ifdef _OPENMP
        std::stringstream name;
        name << "OpenMP Block SSE kernel (" << omp_get_max_threads() << " threads)";
        return name.str();
#else
        return "CPU Block SSE kernel";
#endif
    };


private:
    T *p_real;
    T *p_imag;
    T *r00[2], *r01[2], *r10[2], *r11[2];
    T *i00[2], *i01[2], *i10[2], *i11[2];
    T a;
    T b;
    int sense;

    // NOTE: halo values must be even
    static const size_t HALO_X = 4u;
    static const size_t HALO_Y = 4u;
    // NOTE: block rows must be 16 byte aligned
    //       block height must be even
    static const size_t BLOCK_WIDTH = 128u;
    static const size_t BLOCK_HEIGHT = 128u;
};


/***************
* SSE variants *
***************/
template <int offset_y>
static inline void update_shifty_sse(size_t stride, size_t width, size_t height, float a, float b, float * __restrict__ r1, float * __restrict__ i1, float * __restrict__ r2, float * __restrict__ i2) {
    __m128 aq, bq;
    aq = _mm_load1_ps(&a);
    bq = _mm_load1_ps(&b);
    for (int i = 0; i < height - offset_y; i++) {
        int idx1 = i * stride;
        int idx2 = (i+offset_y) * stride;
        int j = 0;
        for (; j < width - width % 4; j += 4, idx1 += 4, idx2 += 4) {
            __m128 r1q = _mm_load_ps(&r1[idx1]);
            __m128 i1q = _mm_load_ps(&i1[idx1]);
            __m128 r2q = _mm_load_ps(&r2[idx2]);
            __m128 i2q = _mm_load_ps(&i2[idx2]);
            __m128 next_r1q = _mm_sub_ps(_mm_mul_ps(r1q, aq), _mm_mul_ps(i2q, bq));
            __m128 next_i1q = _mm_add_ps(_mm_mul_ps(i1q, aq), _mm_mul_ps(r2q, bq));
            __m128 next_r2q = _mm_sub_ps(_mm_mul_ps(r2q, aq), _mm_mul_ps(i1q, bq));
            __m128 next_i2q = _mm_add_ps(_mm_mul_ps(i2q, aq), _mm_mul_ps(r1q, bq));
            _mm_store_ps(&r1[idx1], next_r1q);
            _mm_store_ps(&i1[idx1], next_i1q);
            _mm_store_ps(&r2[idx2], next_r2q);
            _mm_store_ps(&i2[idx2], next_i2q);
        }
        for (; j < width; ++j, ++idx1, ++idx2) {
            float next_r1 = a * r1[idx1] - b * i2[idx2];
            float next_i1 = a * i1[idx1] + b * r2[idx2];
            float next_r2 = a * r2[idx2] - b * i1[idx1];
            float next_i2 = a * i2[idx2] + b * r1[idx1];
            r1[idx1] = next_r1;
            i1[idx1] = next_i1;
            r2[idx2] = next_r2;
            i2[idx2] = next_i2;
        }
    }
}


template <int offset_y>
static inline void update_shifty_sse(size_t stride, size_t width, size_t height, double a, double b, double * __restrict__ r1, double * __restrict__ i1, double * __restrict__ r2, double * __restrict__ i2) {
    __m128d ap, bp;
    ap = _mm_load1_pd(&a);
    bp = _mm_load1_pd(&b);
    for (int i = 0; i < height - offset_y; i++) {
        int idx1 = i * stride;
        int idx2 = (i+offset_y) * stride;
        int j = 0;
        for (; j < width - width % 2; j += 2, idx1 += 2, idx2 += 2) {
            __m128d r1p = _mm_load_pd(&r1[idx1]);
            __m128d i1p = _mm_load_pd(&i1[idx1]);
            __m128d r2p = _mm_load_pd(&r2[idx2]);
            __m128d i2p = _mm_load_pd(&i2[idx2]);
            __m128d next_r1p = _mm_sub_pd(_mm_mul_pd(r1p, ap), _mm_mul_pd(i2p, bp));
            __m128d next_i1p = _mm_add_pd(_mm_mul_pd(i1p, ap), _mm_mul_pd(r2p, bp));
            __m128d next_r2p = _mm_sub_pd(_mm_mul_pd(r2p, ap), _mm_mul_pd(i1p, bp));
            __m128d next_i2p = _mm_add_pd(_mm_mul_pd(i2p, ap), _mm_mul_pd(r1p, bp));
            _mm_store_pd(&r1[idx1], next_r1p);
            _mm_store_pd(&i1[idx1], next_i1p);
            _mm_store_pd(&r2[idx2], next_r2p);
            _mm_store_pd(&i2[idx2], next_i2p);
        }
        for (; j < width; ++j, ++idx1, ++idx2) {
            double next_r1 = a * r1[idx1] - b * i2[idx2];
            double next_i1 = a * i1[idx1] + b * r2[idx2];
            double next_r2 = a * r2[idx2] - b * i1[idx1];
            double next_i2 = a * i2[idx2] + b * r1[idx1];
            r1[idx1] = next_r1;
            i1[idx1] = next_i1;
            r2[idx2] = next_r2;
            i2[idx2] = next_i2;
        }
    }
}


template <int offset_x>
static inline void update_shiftx_sse(size_t stride, size_t width, size_t height, float a, float b, float * __restrict__ r1, float * __restrict__ i1, float * __restrict__ r2, float * __restrict__ i2) {
    __m128 aq, bq;
    aq = _mm_load1_ps(&a);
    bq = _mm_load1_ps(&b);
    for (int i = 0; i < height; i++) {
        int idx1 = i * stride;
        int idx2 = i * stride + offset_x;
        int j = 0;
        for (; j < width - offset_x - (width - offset_x) % 4; j += 4, idx1 += 4, idx2 += 4) {
            __m128 r1q = _mm_load_ps(&r1[idx1]);
            __m128 i1q = _mm_load_ps(&i1[idx1]);
            __m128 r2q;
            __m128 i2q;
            if (offset_x == 0) {
                r2q = _mm_load_ps(&r2[idx2]);
                i2q = _mm_load_ps(&i2[idx2]);
            } else {
                r2q = _mm_loadu_ps(&r2[idx2]);
                i2q = _mm_loadu_ps(&i2[idx2]);
            }
            __m128 next_r1q = _mm_sub_ps(_mm_mul_ps(r1q, aq), _mm_mul_ps(i2q, bq));
            __m128 next_i1q = _mm_add_ps(_mm_mul_ps(i1q, aq), _mm_mul_ps(r2q, bq));
            __m128 next_r2q = _mm_sub_ps(_mm_mul_ps(r2q, aq), _mm_mul_ps(i1q, bq));
            __m128 next_i2q = _mm_add_ps(_mm_mul_ps(i2q, aq), _mm_mul_ps(r1q, bq));
            _mm_store_ps(&r1[idx1], next_r1q);
            _mm_store_ps(&i1[idx1], next_i1q);
            if (offset_x == 0) {
                _mm_store_ps(&r2[idx2], next_r2q);
                _mm_store_ps(&i2[idx2], next_i2q);
            } else {
                _mm_storeu_ps(&r2[idx2], next_r2q);
                _mm_storeu_ps(&i2[idx2], next_i2q);
            }
        }
        for (; j < width - offset_x; ++j, ++idx1, ++idx2) {
            float next_r1 = a * r1[idx1] - b * i2[idx2];
            float next_i1 = a * i1[idx1] + b * r2[idx2];
            float next_r2 = a * r2[idx2] - b * i1[idx1];
            float next_i2 = a * i2[idx2] + b * r1[idx1];
            r1[idx1] = next_r1;
            i1[idx1] = next_i1;
            r2[idx2] = next_r2;
            i2[idx2] = next_i2;
        }
    }
}


template <int offset_x>
static inline void update_shiftx_sse(size_t stride, size_t width, size_t height, double a, double b, double * __restrict__ r1, double * __restrict__ i1, double * __restrict__ r2, double * __restrict__ i2) {
    __m128d ap, bp;
    ap = _mm_load1_pd(&a);
    bp = _mm_load1_pd(&b);
    for (int i = 0; i < height; i++) {
        int idx1 = i * stride;
        int idx2 = i * stride + offset_x;
        int j = 0;
        for (; j < (width - offset_x) - (width - offset_x) % 2; j += 2, idx1 += 2, idx2 += 2) {
            __m128d r1p = _mm_load_pd(&r1[idx1]);
            __m128d i1p = _mm_load_pd(&i1[idx1]);
            __m128d r2p;
            __m128d i2p;
            if (offset_x == 0) {
                r2p = _mm_load_pd(&r2[idx2]);
                i2p = _mm_load_pd(&i2[idx2]);
            } else {
                r2p = _mm_loadu_pd(&r2[idx2]);
                i2p = _mm_loadu_pd(&i2[idx2]);
            }
            __m128d next_r1p = _mm_sub_pd(_mm_mul_pd(r1p, ap), _mm_mul_pd(i2p, bp));
            __m128d next_i1p = _mm_add_pd(_mm_mul_pd(i1p, ap), _mm_mul_pd(r2p, bp));
            __m128d next_r2p = _mm_sub_pd(_mm_mul_pd(r2p, ap), _mm_mul_pd(i1p, bp));
            __m128d next_i2p = _mm_add_pd(_mm_mul_pd(i2p, ap), _mm_mul_pd(r1p, bp));
            _mm_store_pd(&r1[idx1], next_r1p);
            _mm_store_pd(&i1[idx1], next_i1p);
            if (offset_x == 0) {
                _mm_store_pd(&r2[idx2], next_r2p);
                _mm_store_pd(&i2[idx2], next_i2p);
            } else {
                _mm_storeu_pd(&r2[idx2], next_r2p);
                _mm_storeu_pd(&i2[idx2], next_i2p);
            }
        }
        for (; j < width - offset_x; j++, idx1++, idx2++) {
            double next_r1 = a * r1[idx1] - b * i2[idx2];
            double next_i1 = a * i1[idx1] + b * r2[idx2];
            double next_r2 = a * r2[idx2] - b * i1[idx1];
            double next_i2 = a * i2[idx2] + b * r1[idx1];
            r1[idx1] = next_r1;
            i1[idx1] = next_i1;
            r2[idx2] = next_r2;
            i2[idx2] = next_i2;
        }
    }
}


template <typename T>
static void full_step_sse(size_t stride, size_t width, size_t height, T a, T b, T * r00, T * r01, T * r10, T * r11, T * i00, T * i01, T * i10, T * i11) {
    // 1
    update_shifty_sse<0>(stride, width, height, a, b, r00, i00, r10, i10);
    update_shifty_sse<1>(stride, width, height, a, b, r11, i11, r01, i01);
    // 2
    update_shiftx_sse<0>(stride, width, height, a, b, r00, i00, r01, i01);
    update_shiftx_sse<1>(stride, width, height, a, b, r11, i11, r10, i10);
    // 3
    update_shifty_sse<0>(stride, width, height, a, b, r01, i01, r11, i11);
    update_shifty_sse<1>(stride, width, height, a, b, r10, i10, r00, i00);
    // 4
    update_shiftx_sse<0>(stride, width, height, a, b, r10, i10, r11, i11);
    update_shiftx_sse<1>(stride, width, height, a, b, r01, i01, r00, i00);
    // 4
    update_shiftx_sse<0>(stride, width, height, a, b, r10, i10, r11, i11);
    update_shiftx_sse<1>(stride, width, height, a, b, r01, i01, r00, i00);
    // 3
    update_shifty_sse<0>(stride, width, height, a, b, r01, i01, r11, i11);
    update_shifty_sse<1>(stride, width, height, a, b, r10, i10, r00, i00);
    // 2
    update_shiftx_sse<0>(stride, width, height, a, b, r00, i00, r01, i01);
    update_shiftx_sse<1>(stride, width, height, a, b, r11, i11, r10, i10);
    // 1
    update_shifty_sse<0>(stride, width, height, a, b, r00, i00, r10, i10);
    update_shifty_sse<1>(stride, width, height, a, b, r11, i11, r01, i01);
}



template <typename T, size_t halo_x, size_t block_width, size_t block_height, size_t matrix_width, size_t matrix_height>
static void process_band_sse(size_t read_y, size_t read_height, size_t write_offset, size_t write_height, T a, T b, const T * r00, const T * r01, const T * r10, const T * r11, const T * i00, const T * i01, const T * i10, const T * i11, T * next_r00, T * next_r01, T * next_r10, T * next_r11, T * next_i00, T * next_i01, T * next_i10, T * next_i11) {
    T block_r00[(block_height / 2) * (block_width / 2)];
    T block_r01[(block_height / 2) * (block_width / 2)];
    T block_r10[(block_height / 2) * (block_width / 2)];
    T block_r11[(block_height / 2) * (block_width / 2)];
    T block_i00[(block_height / 2) * (block_width / 2)];
    T block_i01[(block_height / 2) * (block_width / 2)];
    T block_i10[(block_height / 2) * (block_width / 2)];
    T block_i11[(block_height / 2) * (block_width / 2)];

    size_t read_idx;
    size_t read_width;
    size_t block_read_idx;
    size_t write_idx;
    size_t write_width;

    size_t block_stride = (block_width / 2) * sizeof(T);
    size_t matrix_stride = (matrix_width / 2) * sizeof(T);

    if (matrix_width <= block_width) {
        // One full block
        read_idx = (read_y / 2) * (matrix_width / 2);
        read_width = (matrix_width / 2) * sizeof(T);
        memcpy2D(block_r00, block_stride, &r00[read_idx], matrix_stride, read_width, read_height / 2);
        memcpy2D(block_i00, block_stride, &i00[read_idx], matrix_stride, read_width, read_height / 2);
        memcpy2D(block_r01, block_stride, &r01[read_idx], matrix_stride, read_width, read_height / 2);
        memcpy2D(block_i01, block_stride, &i01[read_idx], matrix_stride, read_width, read_height / 2);
        memcpy2D(block_r10, block_stride, &r10[read_idx], matrix_stride, read_width, read_height / 2);
        memcpy2D(block_i10, block_stride, &i10[read_idx], matrix_stride, read_width, read_height / 2);
        memcpy2D(block_r11, block_stride, &r11[read_idx], matrix_stride, read_width, read_height / 2);
        memcpy2D(block_i11, block_stride, &i11[read_idx], matrix_stride, read_width, read_height / 2);

        full_step_sse<T>(block_width / 2, matrix_width / 2, read_height / 2, a, b, block_r00, block_r01, block_r10, block_r11, block_i00, block_i01, block_i10, block_i11);

        block_read_idx = (write_offset / 2) * (block_width / 2);
        write_idx = (read_y / 2 + write_offset / 2) * (matrix_width / 2);
        write_width = read_width;
        memcpy2D(&next_r00[write_idx], matrix_stride, &block_r00[block_read_idx], block_stride, write_width, write_height / 2);
        memcpy2D(&next_i00[write_idx], matrix_stride, &block_i00[block_read_idx], block_stride, write_width, write_height / 2);
        memcpy2D(&next_r01[write_idx], matrix_stride, &block_r01[block_read_idx], block_stride, write_width, write_height / 2);
        memcpy2D(&next_i01[write_idx], matrix_stride, &block_i01[block_read_idx], block_stride, write_width, write_height / 2);
        memcpy2D(&next_r10[write_idx], matrix_stride, &block_r10[block_read_idx], block_stride, write_width, write_height / 2);
        memcpy2D(&next_i10[write_idx], matrix_stride, &block_i10[block_read_idx], block_stride, write_width, write_height / 2);
        memcpy2D(&next_r11[write_idx], matrix_stride, &block_r11[block_read_idx], block_stride, write_width, write_height / 2);
        memcpy2D(&next_i11[write_idx], matrix_stride, &block_i11[block_read_idx], block_stride, write_width, write_height / 2);
    } else {
        // First block [0..block_width - halo_x]
        read_idx = (read_y / 2) * (matrix_width / 2);
        read_width = (block_width / 2) * sizeof(T);
        memcpy2D(block_r00, block_stride, &r00[read_idx], matrix_stride, read_width, read_height / 2);
        memcpy2D(block_i00, block_stride, &i00[read_idx], matrix_stride, read_width, read_height / 2);
        memcpy2D(block_r01, block_stride, &r01[read_idx], matrix_stride, read_width, read_height / 2);
        memcpy2D(block_i01, block_stride, &i01[read_idx], matrix_stride, read_width, read_height / 2);
        memcpy2D(block_r10, block_stride, &r10[read_idx], matrix_stride, read_width, read_height / 2);
        memcpy2D(block_i10, block_stride, &i10[read_idx], matrix_stride, read_width, read_height / 2);
        memcpy2D(block_r11, block_stride, &r11[read_idx], matrix_stride, read_width, read_height / 2);
        memcpy2D(block_i11, block_stride, &i11[read_idx], matrix_stride, read_width, read_height / 2);

        full_step_sse<T>(block_width / 2, block_width / 2, read_height / 2, a, b, block_r00, block_r01, block_r10, block_r11, block_i00, block_i01, block_i10, block_i11);

        block_read_idx = (write_offset / 2) * (block_width / 2);
        write_idx = (read_y / 2 + write_offset / 2) * (matrix_width / 2);
        write_width = ((block_width - halo_x) / 2) * sizeof(T);
        memcpy2D(&next_r00[write_idx], matrix_stride, &block_r00[block_read_idx], block_stride, write_width, write_height / 2);
        memcpy2D(&next_i00[write_idx], matrix_stride, &block_i00[block_read_idx], block_stride, write_width, write_height / 2);
        memcpy2D(&next_r01[write_idx], matrix_stride, &block_r01[block_read_idx], block_stride, write_width, write_height / 2);
        memcpy2D(&next_i01[write_idx], matrix_stride, &block_i01[block_read_idx], block_stride, write_width, write_height / 2);
        memcpy2D(&next_r10[write_idx], matrix_stride, &block_r10[block_read_idx], block_stride, write_width, write_height / 2);
        memcpy2D(&next_i10[write_idx], matrix_stride, &block_i10[block_read_idx], block_stride, write_width, write_height / 2);
        memcpy2D(&next_r11[write_idx], matrix_stride, &block_r11[block_read_idx], block_stride, write_width, write_height / 2);
        memcpy2D(&next_i11[write_idx], matrix_stride, &block_i11[block_read_idx], block_stride, write_width, write_height / 2);

        // Regular blocks in the middle
        size_t block_start;
        read_width = (block_width / 2) * sizeof(T);
        block_read_idx = (write_offset / 2) * (block_width / 2) + halo_x / 2;
        write_width = ((block_width - 2 * halo_x) / 2) * sizeof(T);
        for (block_start = block_width - 2 * halo_x; block_start < matrix_width - block_width; block_start += block_width - 2 * halo_x) {
            read_idx = (read_y / 2) * (matrix_width / 2) + block_start / 2;
            memcpy2D(block_r00, block_stride, &r00[read_idx], matrix_stride, read_width, read_height / 2);
            memcpy2D(block_i00, block_stride, &i00[read_idx], matrix_stride, read_width, read_height / 2);
            memcpy2D(block_r01, block_stride, &r01[read_idx], matrix_stride, read_width, read_height / 2);
            memcpy2D(block_i01, block_stride, &i01[read_idx], matrix_stride, read_width, read_height / 2);
            memcpy2D(block_r10, block_stride, &r10[read_idx], matrix_stride, read_width, read_height / 2);
            memcpy2D(block_i10, block_stride, &i10[read_idx], matrix_stride, read_width, read_height / 2);
            memcpy2D(block_r11, block_stride, &r11[read_idx], matrix_stride, read_width, read_height / 2);
            memcpy2D(block_i11, block_stride, &i11[read_idx], matrix_stride, read_width, read_height / 2);

            full_step_sse<T>(block_width / 2, block_width / 2, read_height / 2, a, b, block_r00, block_r01, block_r10, block_r11, block_i00, block_i01, block_i10, block_i11);

            write_idx = (read_y / 2 + write_offset / 2) * (matrix_width / 2) + (block_start + halo_x) / 2;
            memcpy2D(&next_r00[write_idx], matrix_stride, &block_r00[block_read_idx], block_stride, write_width, write_height / 2);
            memcpy2D(&next_i00[write_idx], matrix_stride, &block_i00[block_read_idx], block_stride, write_width, write_height / 2);
            memcpy2D(&next_r01[write_idx], matrix_stride, &block_r01[block_read_idx], block_stride, write_width, write_height / 2);
            memcpy2D(&next_i01[write_idx], matrix_stride, &block_i01[block_read_idx], block_stride, write_width, write_height / 2);
            memcpy2D(&next_r10[write_idx], matrix_stride, &block_r10[block_read_idx], block_stride, write_width, write_height / 2);
            memcpy2D(&next_i10[write_idx], matrix_stride, &block_i10[block_read_idx], block_stride, write_width, write_height / 2);
            memcpy2D(&next_r11[write_idx], matrix_stride, &block_r11[block_read_idx], block_stride, write_width, write_height / 2);
            memcpy2D(&next_i11[write_idx], matrix_stride, &block_i11[block_read_idx], block_stride, write_width, write_height / 2);
        }
        // Last block
        read_idx = (read_y / 2) * (matrix_width / 2) + block_start / 2;
        read_width = (matrix_width / 2 - block_start / 2) * sizeof(T);
        memcpy2D(block_r00, block_stride, &r00[read_idx], matrix_stride, read_width, read_height / 2);
        memcpy2D(block_i00, block_stride, &i00[read_idx], matrix_stride, read_width, read_height / 2);
        memcpy2D(block_r01, block_stride, &r01[read_idx], matrix_stride, read_width, read_height / 2);
        memcpy2D(block_i01, block_stride, &i01[read_idx], matrix_stride, read_width, read_height / 2);
        memcpy2D(block_r10, block_stride, &r10[read_idx], matrix_stride, read_width, read_height / 2);
        memcpy2D(block_i10, block_stride, &i10[read_idx], matrix_stride, read_width, read_height / 2);
        memcpy2D(block_r11, block_stride, &r11[read_idx], matrix_stride, read_width, read_height / 2);
        memcpy2D(block_i11, block_stride, &i11[read_idx], matrix_stride, read_width, read_height / 2);

        full_step_sse<T>(block_width / 2, matrix_width / 2 - block_start / 2, read_height / 2, a, b, block_r00, block_r01, block_r10, block_r11, block_i00, block_i01, block_i10, block_i11);

        block_read_idx = (write_offset / 2) * (block_width / 2) + halo_x / 2;
        write_idx = (read_y / 2 + write_offset / 2) * (matrix_width / 2) + (block_start + halo_x) / 2;
        write_width = (matrix_width / 2 - block_start / 2 - halo_x / 2) * sizeof(T);
        memcpy2D(&next_r00[write_idx], matrix_stride, &block_r00[block_read_idx], block_stride, write_width, write_height / 2);
        memcpy2D(&next_i00[write_idx], matrix_stride, &block_i00[block_read_idx], block_stride, write_width, write_height / 2);
        memcpy2D(&next_r01[write_idx], matrix_stride, &block_r01[block_read_idx], block_stride, write_width, write_height / 2);
        memcpy2D(&next_i01[write_idx], matrix_stride, &block_i01[block_read_idx], block_stride, write_width, write_height / 2);
        memcpy2D(&next_r10[write_idx], matrix_stride, &block_r10[block_read_idx], block_stride, write_width, write_height / 2);
        memcpy2D(&next_i10[write_idx], matrix_stride, &block_i10[block_read_idx], block_stride, write_width, write_height / 2);
        memcpy2D(&next_r11[write_idx], matrix_stride, &block_r11[block_read_idx], block_stride, write_width, write_height / 2);
        memcpy2D(&next_i11[write_idx], matrix_stride, &block_i11[block_read_idx], block_stride, write_width, write_height / 2);
    }
}


template <typename T, int matrix_width, int matrix_height>
CPUBlockSSEKernel<T, matrix_width,matrix_height>::CPUBlockSSEKernel(T *_p_real, T *_p_imag, T _a, T _b):
    p_real(_p_real),
    p_imag(_p_imag),
    a(_a),
    b(_b),
    sense(0)
{
    assert (matrix_width % 2 == 0);
    assert (matrix_height % 2 == 0);

    posix_memalign(reinterpret_cast<void**>(&r00[0]), 64, ((matrix_width * matrix_height) / 4) * sizeof(T));
    posix_memalign(reinterpret_cast<void**>(&r00[1]), 64, ((matrix_width * matrix_height) / 4) * sizeof(T));
    posix_memalign(reinterpret_cast<void**>(&r01[0]), 64, ((matrix_width * matrix_height) / 4) * sizeof(T));
    posix_memalign(reinterpret_cast<void**>(&r01[1]), 64, ((matrix_width * matrix_height) / 4) * sizeof(T));
    posix_memalign(reinterpret_cast<void**>(&r10[0]), 64, ((matrix_width * matrix_height) / 4) * sizeof(T));
    posix_memalign(reinterpret_cast<void**>(&r10[1]), 64, ((matrix_width * matrix_height) / 4) * sizeof(T));
    posix_memalign(reinterpret_cast<void**>(&r11[0]), 64, ((matrix_width * matrix_height) / 4) * sizeof(T));
    posix_memalign(reinterpret_cast<void**>(&r11[1]), 64, ((matrix_width * matrix_height) / 4) * sizeof(T));
    posix_memalign(reinterpret_cast<void**>(&i00[0]), 64, ((matrix_width * matrix_height) / 4) * sizeof(T));
    posix_memalign(reinterpret_cast<void**>(&i00[1]), 64, ((matrix_width * matrix_height) / 4) * sizeof(T));
    posix_memalign(reinterpret_cast<void**>(&i01[0]), 64, ((matrix_width * matrix_height) / 4) * sizeof(T));
    posix_memalign(reinterpret_cast<void**>(&i01[1]), 64, ((matrix_width * matrix_height) / 4) * sizeof(T));
    posix_memalign(reinterpret_cast<void**>(&i10[0]), 64, ((matrix_width * matrix_height) / 4) * sizeof(T));
    posix_memalign(reinterpret_cast<void**>(&i10[1]), 64, ((matrix_width * matrix_height) / 4) * sizeof(T));
    posix_memalign(reinterpret_cast<void**>(&i11[0]), 64, ((matrix_width * matrix_height) / 4) * sizeof(T));
    posix_memalign(reinterpret_cast<void**>(&i11[1]), 64, ((matrix_width * matrix_height) / 4) * sizeof(T));
    for (int i = 0; i < matrix_height/2; i++) {
        for (int j = 0; j < matrix_width/2; j++) {
            r00[0][i * matrix_width / 2 + j] = p_real[2 * i * matrix_width + 2 * j];
            i00[0][i * matrix_width / 2 + j] = p_imag[2 * i * matrix_width + 2 * j];
            r01[0][i * matrix_width / 2 + j] = p_real[2 * i * matrix_width + 2 * j + 1];
            i01[0][i * matrix_width / 2 + j] = p_imag[2 * i * matrix_width + 2 * j + 1];
        }
        for (int j = 0; j < matrix_width/2; j++) {
            r10[0][i * matrix_width / 2 + j] = p_real[(2 * i + 1) * matrix_width + 2 * j];
            i10[0][i * matrix_width / 2 + j] = p_imag[(2 * i + 1) * matrix_width + 2 * j];
            r11[0][i * matrix_width / 2 + j] = p_real[(2 * i + 1) * matrix_width + 2 * j + 1];
            i11[0][i * matrix_width / 2 + j] = p_imag[(2 * i + 1) * matrix_width + 2 * j + 1];
        }
    }
}


template <typename T, int matrix_width, int matrix_height>
CPUBlockSSEKernel<T,matrix_width,matrix_height>::~CPUBlockSSEKernel() {
    free(r00[0]);
    free(r00[1]);
    free(r01[0]);
    free(r01[1]);
    free(r10[0]);
    free(r10[1]);
    free(r11[0]);
    free(r11[1]);
    free(i00[0]);
    free(i00[1]);
    free(i01[0]);
    free(i01[1]);
    free(i10[0]);
    free(i10[1]);
    free(i11[0]);
    free(i11[1]);
}


template <typename T, int matrix_width, int matrix_height>
void CPUBlockSSEKernel<T,matrix_width,matrix_height>::run_kernels() {

    if (matrix_height <= BLOCK_HEIGHT) {
        // One full band
        process_band_sse<T, HALO_X, BLOCK_WIDTH, BLOCK_HEIGHT, matrix_width, matrix_height>(0, matrix_height, 0, matrix_height, a, b, r00[sense], r01[sense], r10[sense], r11[sense], i00[sense], i01[sense], i10[sense], i11[sense], r00[1-sense], r01[1-sense], r10[1-sense], r11[1-sense], i00[1-sense], i01[1-sense], i10[1-sense], i11[1-sense]);
    } else {

#ifdef _OPENMP
        size_t block_start;
        #pragma omp parallel default(shared) private(block_start)
        {
            #pragma omp sections nowait
            {
                #pragma omp section
                {
                    // First band
                    process_band_sse<T, HALO_X, BLOCK_WIDTH, BLOCK_HEIGHT, matrix_width, matrix_height>(0, BLOCK_HEIGHT, 0, BLOCK_HEIGHT - HALO_Y, a, b, r00[sense], r01[sense], r10[sense], r11[sense], i00[sense], i01[sense], i10[sense], i11[sense], r00[1-sense], r01[1-sense], r10[1-sense], r11[1-sense], i00[1-sense], i01[1-sense], i10[1-sense], i11[1-sense]);
                }
                #pragma omp section
                {
                    // Last band
                    block_start = matrix_height - BLOCK_HEIGHT + (matrix_height - BLOCK_HEIGHT) % (BLOCK_HEIGHT - 2 * HALO_Y);
                    process_band_sse<T, HALO_X, BLOCK_WIDTH, BLOCK_HEIGHT, matrix_width, matrix_height>(block_start, matrix_height - block_start, HALO_Y, matrix_height - block_start - HALO_Y, a, b, r00[sense], r01[sense], r10[sense], r11[sense], i00[sense], i01[sense], i10[sense], i11[sense], r00[1-sense], r01[1-sense], r10[1-sense], r11[1-sense], i00[1-sense], i01[1-sense], i10[1-sense], i11[1-sense]);
                }
            }

            #pragma omp for schedule(runtime) nowait
            for (block_start = BLOCK_HEIGHT - 2 * HALO_Y; block_start < matrix_height - BLOCK_HEIGHT; block_start += BLOCK_HEIGHT - 2 * HALO_Y) {
                process_band_sse<T, HALO_X, BLOCK_WIDTH, BLOCK_HEIGHT, matrix_width, matrix_height>(block_start, BLOCK_HEIGHT, HALO_Y, BLOCK_HEIGHT - 2 * HALO_Y, a, b, r00[sense], r01[sense], r10[sense], r11[sense], i00[sense], i01[sense], i10[sense], i11[sense], r00[1-sense], r01[1-sense], r10[1-sense], r11[1-sense], i00[1-sense], i01[1-sense], i10[1-sense], i11[1-sense]);
            }

            #pragma omp barrier
        }
#else
        // First band
        process_band_sse<T, HALO_X, BLOCK_WIDTH, BLOCK_HEIGHT, matrix_width, matrix_height>(0, BLOCK_HEIGHT, 0, BLOCK_HEIGHT - HALO_Y, a, b, r00[sense], r01[sense], r10[sense], r11[sense], i00[sense], i01[sense], i10[sense], i11[sense], r00[1-sense], r01[1-sense], r10[1-sense], r11[1-sense], i00[1-sense], i01[1-sense], i10[1-sense], i11[1-sense]);

        size_t block_start;
        for (block_start = BLOCK_HEIGHT - 2 * HALO_Y; block_start < matrix_height - BLOCK_HEIGHT; block_start += BLOCK_HEIGHT - 2 * HALO_Y) {
            process_band_sse<T, HALO_X, BLOCK_WIDTH, BLOCK_HEIGHT, matrix_width, matrix_height>(block_start, BLOCK_HEIGHT, HALO_Y, BLOCK_HEIGHT - 2 * HALO_Y, a, b, r00[sense], r01[sense], r10[sense], r11[sense], i00[sense], i01[sense], i10[sense], i11[sense], r00[1-sense], r01[1-sense], r10[1-sense], r11[1-sense], i00[1-sense], i01[1-sense], i10[1-sense], i11[1-sense]);
        }

        // Last band
        process_band_sse<T, HALO_X, BLOCK_WIDTH, BLOCK_HEIGHT, matrix_width, matrix_height>(block_start, matrix_height - block_start, HALO_Y, matrix_height - block_start - HALO_Y, a, b, r00[sense], r01[sense], r10[sense], r11[sense], i00[sense], i01[sense], i10[sense], i11[sense], r00[1-sense], r01[1-sense], r10[1-sense], r11[1-sense], i00[1-sense], i01[1-sense], i10[1-sense], i11[1-sense]);
#endif /* _OPENMP */
    }

    sense = 1 - sense;
}


template <typename T, int matrix_width, int matrix_height>
void CPUBlockSSEKernel<T,matrix_width,matrix_height>::wait_for_completion() {
}


template <typename T, int matrix_width, int matrix_height>
void CPUBlockSSEKernel<T, matrix_width,matrix_height>::get_sample(size_t dest_stride, size_t x, size_t y, size_t width, size_t height, T * dest_real, T * dest_imag) const {
    get_quadrant_sample<T>(r00[sense], r01[sense], r10[sense], r11[sense], i00[sense], i01[sense], i10[sense], i11[sense], matrix_width / 2, dest_stride, x, y, width, height, dest_real, dest_imag);
}

#endif
