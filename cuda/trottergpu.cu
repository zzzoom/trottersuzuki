#include <cuda.h>
#include <helper_cuda.h>
#include <stdint.h>
#include <sys/time.h>

#include <algorithm>
#include <complex>
#include <cmath>
#include <cstdio>
#include <cstring>
#include <iostream>
#include <fstream>
#include <vector>

#include "../settings.h"
#include "../trotterkernel.h"
#include "../common.h"

#include "cc2kernel.h"

#ifdef COMPARE_CPU
#include "../reference/cpukernel.h"
#endif


template <typename T>
void trotter() {

    // alloc memory for p and the reference kernel
    T * p_real = new T[MATRIX_WIDTH * MATRIX_HEIGHT];
    T * p_imag = new T[MATRIX_WIDTH * MATRIX_HEIGHT];

    // alloc memory for snapshots
    T * samples_real;
    T * samples_imag;
    checkCudaErrors(cudaHostAlloc(reinterpret_cast<void**>(&samples_real), SAMPLES * SNAPSHOT_WIDTH * SNAPSHOT_HEIGHT * sizeof(T), 0));
    checkCudaErrors(cudaHostAlloc(reinterpret_cast<void**>(&samples_imag), SAMPLES * SNAPSHOT_WIDTH * SNAPSHOT_HEIGHT * sizeof(T), 0));

    // init p
    init_p(p_real, p_imag, MATRIX_WIDTH, MATRIX_HEIGHT);

    // init kernels
    typedef ITrotterKernel<T> kernel_t;
    typedef typename std::vector<kernel_t *> kernel_vector_t;
    kernel_vector_t kernels;
    kernels.push_back(new CC2Kernel<T, MATRIX_WIDTH, MATRIX_HEIGHT>(p_real, p_imag, h_a, h_b));

    struct timeval start, end;

#ifdef COMPARE_CPU
    T * reference_real  = new T[MATRIX_WIDTH * MATRIX_HEIGHT];
    T * reference_imag  = new T[MATRIX_WIDTH * MATRIX_HEIGHT];
    T * refsamples_real = new T[SAMPLES * SNAPSHOT_WIDTH * SNAPSHOT_HEIGHT];
    T * refsamples_imag = new T[SAMPLES * SNAPSHOT_WIDTH * SNAPSHOT_HEIGHT];
    std::copy(p_real, p_real + MATRIX_WIDTH * MATRIX_HEIGHT, reference_real);
    std::copy(p_imag, p_imag + MATRIX_WIDTH * MATRIX_HEIGHT, reference_imag);

    kernel_t *cpukernel = new CPUKernel<T, MATRIX_WIDTH, MATRIX_HEIGHT>(reference_real, reference_imag, h_a, h_b);
    gettimeofday(&start, NULL);
    for (int i = 0; i < ITERATIONS; i++) {
        if (i % SNAPSHOT_EVERY == 0) {
            // TODO?: use a ring buffer and write to disk on the fly
            cpukernel->wait_for_completion();
            // cpukernel->get_sample(&refsamples[(i/SNAPSHOT_EVERY) * SNAPSHOT_WIDTH * SNAPSHOT_HEIGHT], SNAPSHOT_X, SNAPSHOT_Y, SNAPSHOT_WIDTH, SNAPSHOT_HEIGHT);
        }
        cpukernel->run_kernels();
    }
    cpukernel->wait_for_completion();
    gettimeofday(&end, NULL);
    long reftime = (end.tv_sec - start.tv_sec) * 1000000 + (end.tv_usec - start.tv_usec);
    std::cout << MATRIX_WIDTH << "x" << MATRIX_HEIGHT << " " << cpukernel->get_name() << " " << reftime << std::endl;
#endif

    for (typename kernel_vector_t::iterator it = kernels.begin(); it != kernels.end(); ++it) {
        kernel_t * kernel = *it;
        gettimeofday(&start, NULL);
        for (int i = 0; i < ITERATIONS; i++) {
            if (i % SNAPSHOT_EVERY == 0) {
                // TODO?: use a ring buffer and write to disk on the fly
                kernel->wait_for_completion();
                // kernel->get_sample(&samples[(i/SNAPSHOT_EVERY) * SNAPSHOT_WIDTH * SNAPSHOT_HEIGHT], SNAPSHOT_X, SNAPSHOT_Y, SNAPSHOT_WIDTH, SNAPSHOT_HEIGHT);
            }
            kernel->run_kernels();
        }
        kernel->wait_for_completion();
        gettimeofday(&end, NULL);
        long time = (end.tv_sec - start.tv_sec) * 1000000 + (end.tv_usec - start.tv_usec);
#ifdef COMPARE_CPU
        std::cout << MATRIX_WIDTH << "x" << MATRIX_HEIGHT << " " << kernel->get_name() << " " << time << " (" << static_cast<double>(reftime) / time << "x)" << std::endl;
        kernel->get_sample(MATRIX_WIDTH, 0, 0, MATRIX_WIDTH, MATRIX_HEIGHT, p_real, p_imag);
        measure_error<T>(reference_real, reference_imag, p_real, p_imag, MATRIX_WIDTH, MATRIX_WIDTH, MATRIX_HEIGHT);
#else
        std::cout << MATRIX_WIDTH << "x" << MATRIX_HEIGHT << " " << kernel->get_name() << " " << MATRIX_WIDTH * MATRIX_HEIGHT << " "<< time << std::endl;
#endif
    }

#ifdef COMPARE_CPU
    delete[] reference_real;
    delete[] reference_imag;
    delete[] refsamples_real;
    delete[] refsamples_imag;
    delete cpukernel;
#endif
    delete[] p_real;
    delete[] p_imag;
    checkCudaErrors(cudaFreeHost(samples_real));
    checkCudaErrors(cudaFreeHost(samples_imag));
}

int main(int argc, char** argv) {
    trotter<PRECISION>();
    return 0;
}
