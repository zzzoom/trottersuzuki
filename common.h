#ifndef __COMMON_H
#define __COMMON_H

#include <cassert>
#include <cmath>
#include <complex>
#include <string>
#include <iostream>
#include <fstream>

/*
void dumpPPM(const char * filename, float2 *matrix, int width, int height, float max);
float find_max_abs(float2 *matrix, int n);
void measure_error(std::string filename, float2 *reference, float2 *result);
void print_error(float2 *reference, float2 *result);
*/

/*

void dumpPPM(const char * filename, float2 *matrix, int width, int height, float max) {
    FILE *out;
    out = fopen(filename, "w");
    fprintf(out, "P6 %u %u %u\n", width, height, 255u);
    uint8_t rgb[3];
    float2 *cursor = matrix;
    for (int i = 0; i < height; i++) {
        for (int j = 0; j < width; j++, cursor++) {
            float weight = std::abs(std::complex<float>(cursor->x, cursor->y)) / max;
            rgb[0] = static_cast<uint8_t>(255 * weight);
            rgb[1] = static_cast<uint8_t>(255 * weight);
            rgb[2] = static_cast<uint8_t>(255 * weight);
            fwrite((void*)rgb, 3, 1, out);
        }
    }
    fclose(out);
}


float find_max_abs(float2 *matrix, int n) {
    float max = 0.0f;
    for (int i = 0; i < n; i++) {
        float v = std::abs(std::complex<float>(matrix[i].x, matrix[i].y));
        if (v > max) {
            max = v;
        }
    }
    return max;
}


void print_error(float2 *reference, float2 *result) {
    double maxabs = 0.0;
    int abspos = 0;
    double maxrel = 0.0;
    int relpos = 0;

    for (int i = 0; i < MATRIX_HEIGHT * MATRIX_WIDTH; i++) {
        double abs = fabs((double) result[i].x - (double) reference[i].x);
        if (abs > maxabs) {
            abspos = i;
            maxabs = abs;
        }
        double rel = abs / fabs(fmax((double) result[i].x, (double) reference[i].x));
        if (rel > maxrel) {
            relpos = i;
            maxrel = rel;
        }
        abs = fabs((double) result[i].y - (double) reference[i].y);
        if (abs > maxabs) {
            abspos = i;
            maxabs = abs;
        }
        rel = abs / fabs(fmax((double) result[i].y, (double) reference[i].y));
        if (rel > maxrel) {
            relpos = i;
            maxrel = rel;
        }
    }
    std::cout << "absE: " << maxabs << " (pos " << abspos << ") relE: " << maxrel << " (pos " << relpos << ")" << std::endl;
}
*/

template <typename T>
void measure_error(const T * ref_real, const T * ref_imag, const T * other_real, const T * other_imag, size_t stride, size_t width, size_t height) {
    double maxabs = 0.0;
    int abspos = 0;
    double maxrel = 0.0;
    int relpos = 0;

    double abs_r, abs_i, rel_r, rel_i;

    for (size_t i = 0; i < height; i++) {
        for (size_t j = 0; j < width; j++) {
            size_t idx = i * stride + j;
            abs_r = fabs((double) ref_real[idx] - (double) other_real[idx]);
            if (abs_r != 0.0) {
                rel_r = abs_r / fabs(fmax((double) ref_real[idx], (double) other_real[idx]));
                if (abs_r > maxabs) {
                    abspos = i;
                    maxabs = abs_r;
                }
                if (rel_r > maxrel) {
                    relpos = i;
                    maxrel = rel_r;
                }
            }

            abs_i = fabs((double) ref_imag[idx] - (double) other_imag[idx]);
            if (abs_i != 0.0) {
                rel_i = abs_i / fabs(fmax((double) ref_imag[idx], (double) other_imag[idx]));
                if (abs_i > maxabs) {
                    abspos = i;
                    maxabs = abs_i;
                }
                if (rel_i > maxrel) {
                    relpos = i;
                    maxrel = rel_i;
                }
            }
/*
            if (abs_r > 0.000001 || abs_i > 0.000001) {
                std::cout << "(" << j << "," << i << "):\tr " <<
                    ref_real[idx] << "/" << other_real[idx] << "\ti " <<
                    ref_imag[idx] << "/" << other_imag[idx] << " abs " <<
                    abs_r << " + " << abs_i << "i" << std::endl;
            }
*/
        }
    }
    std::cout << " absE: " << maxabs << " (pos " << abspos << ") relE: " << maxrel << " (pos " << relpos << ")" << std::endl;
}


template <typename T>
void print_complex_matrix(std::string filename, T * matrix_real, T * matrix_imag, size_t stride, size_t width, size_t height) {
    std::ofstream out(filename.c_str(), std::ios::out | std::ios::trunc);
    for (size_t i = 0; i < height; ++i) {
        for (size_t j = 0; j < width; ++j) {
            out << "(" << matrix_real[i * stride + j] << "," << matrix_imag[i * stride + j] << ") ";
            out << std::endl;
        }
    }
    out.close();
}

template <typename T>
static void init_p(T *p_real, T *p_imag, int matrix_width, int matrix_height) {
    double s = 64.0; // FIXME: y esto?

    for (int j = 1; j <= matrix_height; j++) {
        for (int i = 1; i <= matrix_width; i++) {
            // p(i,j)=exp(-((i-180.)**2+(j-300.)**2)/(2*s**2))*exp(im*0.4*(i+j-480.))
            std::complex<T> tmp = std::complex<T>(exp(-(pow(i - 180.0, 2.0) + pow(j - 300.0, 2.0)) / (2.0 * pow(s, 2.0))), 0.0)
                                * exp(std::complex<T>(0.0, 0.4 * (i + j - 480.0)));

            p_real[(j-1) * matrix_width + i-1] = real(tmp);
            p_imag[(j-1) * matrix_width + i-1] = imag(tmp);
        }
    }
}

static void memcpy2D(void * dst, size_t dstride, const void * src, size_t sstride, size_t width, size_t height) {
    char *d = reinterpret_cast<char *>(dst);
    const char *s = reinterpret_cast<const char *>(src);
    for (size_t i = 0; i < height; ++i) {
        for (size_t j = 0; j < width; ++j) {
            d[i * dstride + j] = s[i * sstride + j];
        }
    }
}


/*
template <typename T, class Kernel, int matrix_width, int matrix_height>
long benchmark_trotter(T * p_real, T * p_imag, T a, T b, unsigned int iterations) {

    T * real = new T[matrix_width * matrix_height];
    T * imag = new T[matrix_width * matrix_height];
    std::copy(p_real, p_real + (matrix_width * matrix_height), real);
    std::copy(p_imag, p_imag + (matrix_width * matrix_height), imag);

    ITrotterKernel<T> kernel = new Kernel<T, matrix_width, matrix_height>(real, imag, a, b);

    struct timeval start;
    gettimeofday(&start, NULL);

    for (unsigned int i = 0; i < iterations; i++) {
        kernel->run_kernels();
    }
    kernel->wait_for_completion();

    struct timeval end;
    gettimeofday(&end, NULL);

    delete[] real;
    delete[] imag;

    return (end.tv_sec - start.tv_sec) * 1000000 + (end.tv_usec - start.tv_usec);
}

template <typename T, class Kernel, int matrix_width, int matrix_height>
void compare_trotter(T * p_real, T * p_imag, T a, T b, unsigned int iterations) {

    T * real = new T[matrix_width * matrix_height];
    T * imag = new T[matrix_width * matrix_height];
    std::copy(p_real, p_real + (matrix_width * matrix_height), real);
    std::copy(p_imag, p_imag + (matrix_width * matrix_height), imag);

    ITrotterKernel<T> kernel = new Kernel<T, matrix_width, matrix_height>(real, imag, a, b);

    for (unsigned int i = 0; i < iterations; i++) {
        kernel->run_kernels();
    }
    kernel->wait_for_completion();

    if (!kernel->runs_in_place()) {
        kernel->get_sample(real, imag, 0, 0, matrix_width, matrix_height);
    }

    T * ref_real = new T[matrix_width * matrix_height];
    T * ref_imag = new T[matrix_width * matrix_height];
    std::copy(p_real, p_real + (matrix_width * matrix_height), ref_real);
    std::copy(p_imag, p_imag + (matrix_width * matrix_height), ref_imag);

    ITrotterKernel<T> * ref_kernel = new CPUKernel<T, matrix_width, matrix_height>(ref_real, ref_imag, a, b);
    for (unsigned int i = 0; i < iterations; i++) {
        ref_kernel->run_kernels();
    }
    ref_kernel->wait_for_completion();

    if (!ref_kernel->runs_in_place()) {
        ref_kernel->get_sample(ref_real, ref_imag, 0, 0, matrix_width, matrix_height);
    }
    measure_error<T>(ref_real, ref_imag, real, imag, matrix_width, matrix_width, matrix_height);

    delete kernel;
    delete ref_kernel;
    delete[] real;
    delete[] imag;
    delete[] ref_real;
    delete[] ref_imag;
}
*/

template <typename T>
static inline void merge_line(const T * evens, const T * odds, size_t x, size_t width, T * dest) {

    const T * odds_p = odds + (x/2);
    const T * evens_p = evens + (x/2);

    size_t dest_x = x;
    if (x % 2 == 1) {
        dest[dest_x++] = *(odds_p++);
    }
    while (dest_x < (x + width) - (x + width) % 2) {
        dest[dest_x++] = *(evens_p++);
        dest[dest_x++] = *(odds_p++);
    }
    if (dest_x < x + width) {
        dest[dest_x++] = *evens_p;
    }
    assert(dest_x == x + width);
}


template <typename T>
static inline void get_quadrant_sample(const T * r00, const T * r01, const T * r10, const T * r11,
                                       const T * i00, const T * i01, const T * i10, const T * i11,
                                       size_t src_stride, size_t dest_stride,
                                       size_t x, size_t y, size_t width, size_t height,
                                       T * dest_real, T * dest_imag)
{
    size_t dest_y = y;
    if (y % 2 == 1) {
        merge_line<T>(&r10[(y/2) * src_stride], &r11[(y/2) * src_stride], x, width, dest_real);
        merge_line<T>(&i10[(y/2) * src_stride], &i11[(y/2) * src_stride], x, width, dest_imag);
        ++dest_y;
    }
    while (dest_y < (y + height) - (y + height) % 2) {
        merge_line<T>(&r00[(dest_y/2) * src_stride], &r01[(dest_y/2) * src_stride], x, width, &dest_real[dest_y * dest_stride]);
        merge_line<T>(&i00[(dest_y/2) * src_stride], &i01[(dest_y/2) * src_stride], x, width, &dest_imag[dest_y * dest_stride]);
        ++dest_y;
        merge_line<T>(&r10[(dest_y/2) * src_stride], &r11[(dest_y/2) * src_stride], x, width, &dest_real[dest_y * dest_stride]);
        merge_line<T>(&i10[(dest_y/2) * src_stride], &i11[(dest_y/2) * src_stride], x, width, &dest_imag[dest_y * dest_stride]);
        ++dest_y;
    }
    if (dest_y < y + height) {
        merge_line<T>(&r00[(dest_y/2) * src_stride], &r01[(dest_y/2) * src_stride], x, width, &dest_real[dest_y * dest_stride]);
        merge_line<T>(&i00[(dest_y/2) * src_stride], &i01[(dest_y/2) * src_stride], x, width, &dest_imag[dest_y * dest_stride]);
    }
    assert (dest_y == y + height);
}


#endif
