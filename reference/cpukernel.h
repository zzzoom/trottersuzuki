#ifndef __CPUKERNEL_H
#define __CPUKERNEL_H

#include "../trotterkernel.h"

template <typename T, int matrix_width, int matrix_height>
class CPUKernel: public ITrotterKernel<T> {
public:
    CPUKernel(T *p_real, T *p_imag, T a, T b);
    ~CPUKernel();
    void run_kernels();
    void run_kernel(int k);
    void wait_for_completion();
    void copy_results();
    void get_sample(size_t dest_stride, size_t x, size_t y, size_t width, size_t height, T * dest_real, T * dest_imag) const;

    bool runs_in_place() const { return true; }
    std::string get_name() const { return "CPU kernel"; }

private:
    void kernel1(T *p_real, T *p_imag);
    void kernel2(T *p_real, T *p_imag);
    void kernel3(T *p_real, T *p_imag);
    void kernel4(T *p_real, T *p_imag);

    T *p_real;
    T *p_imag;
    T a;
    T b;
};


template <typename T, int matrix_width, int matrix_height>
CPUKernel<T, matrix_width, matrix_height>::CPUKernel(T *_p_real, T *_p_imag, T _a, T _b):
    p_real(_p_real),
    p_imag(_p_imag),
    a(_a),
    b(_b)
{
}

template <typename T, int matrix_width, int matrix_height>
CPUKernel<T, matrix_width, matrix_height>::~CPUKernel() { }

template <typename T, int matrix_width, int matrix_height>
void CPUKernel<T, matrix_width, matrix_height>::run_kernel(int k) {
    switch (k) {
    case 1:
        kernel1(p_real, p_imag);
        break;
    case 2:
        kernel2(p_real, p_imag);
        break;
    case 3:
        kernel3(p_real, p_imag);
        break;
    case 4:
        kernel4(p_real, p_imag);
        break;
    default:
        // TODO: throw
        break;
    }
}

template <typename T, int matrix_width, int matrix_height>
void CPUKernel<T, matrix_width, matrix_height>::run_kernels() {
    kernel1(p_real, p_imag);
    kernel2(p_real, p_imag);
    kernel3(p_real, p_imag);
    kernel4(p_real, p_imag);
    kernel4(p_real, p_imag);
    kernel3(p_real, p_imag);
    kernel2(p_real, p_imag);
    kernel1(p_real, p_imag);
}

template <typename T, int matrix_width, int matrix_height>
void CPUKernel<T, matrix_width, matrix_height>::wait_for_completion() { }

template <typename T, int matrix_width, int matrix_height>
void CPUKernel<T, matrix_width, matrix_height>::copy_results() { }


template <typename T, int matrix_width, int matrix_height>
void CPUKernel<T, matrix_width,matrix_height>::get_sample(size_t dest_stride, size_t x, size_t y, size_t width, size_t height, T * dest_real, T * dest_imag) const {
    memcpy2D(dest_real, dest_stride * sizeof(T), &(p_real[y * matrix_width + x]), matrix_width * sizeof(T), width * sizeof(T), height);
    memcpy2D(dest_imag, dest_stride * sizeof(T), &(p_imag[y * matrix_width + x]), matrix_width * sizeof(T), width * sizeof(T), height);
}

// Vertical kernels ------------------------------------------------------------
template <typename T, int matrix_width, int matrix_height>
void CPUKernel<T, matrix_width, matrix_height>::kernel1(T *p_real, T *p_imag) {
    for (int x = 0, peer = matrix_width; x < matrix_width; x += 2, peer += 2) {
        T tmp_real = p_real[x];
        T tmp_imag = p_imag[x];
        p_real[x] = a * tmp_real - b * p_imag[peer];
        p_imag[x] = a * tmp_imag + b * p_real[peer];
        p_real[peer] = a * p_real[peer] - b * tmp_imag;
        p_imag[peer] = a * p_imag[peer] + b * tmp_real;
    }
    for (int y = 1; y < matrix_height - 1; y++) {
        for (int idx = y * matrix_width + y % 2, peer = idx + matrix_width; idx < (y + 1) * matrix_width; idx += 2, peer += 2) {
            T tmp_real = p_real[idx];
            T tmp_imag = p_imag[idx];
            p_real[idx] = a * p_real[idx] - b * p_imag[peer];
            p_imag[idx] = a * p_imag[idx] + b * p_real[peer];
            p_real[peer] = a * p_real[peer] - b * tmp_imag;
            p_imag[peer] = a * p_imag[peer] + b * tmp_real;
        }
    }
}

template <typename T, int matrix_width, int matrix_height>
void CPUKernel<T, matrix_width, matrix_height>::kernel3(T *p_real, T *p_imag) {
    for (int x = 1, peer = matrix_width + 1; x < matrix_width; x += 2, peer += 2) {
        float tmp_real = p_real[x];
        float tmp_imag = p_imag[x];
        p_real[x] = a * tmp_real - b * p_imag[peer];
        p_imag[x] = a * tmp_imag + b * p_real[peer];
        p_real[peer] = a * p_real[peer] - b * tmp_imag;
        p_imag[peer] = a * p_imag[peer] + b * tmp_real;
    }
    for (int y = 1; y < matrix_height - 1; y++) {
        for (int idx = y * matrix_width + 1 - y % 2, peer = idx + matrix_width; idx < (y + 1) * matrix_width; idx += 2, peer += 2) {
            float tmp_real = p_real[idx];
            float tmp_imag = p_imag[idx];
            p_real[idx] = a * tmp_real - b * p_imag[peer];
            p_imag[idx] = a * tmp_imag + b * p_real[peer];
            p_real[peer] = a * p_real[peer] - b * tmp_imag;
            p_imag[peer] = a * p_imag[peer] + b * tmp_real;
        }
    }
}

// Horizontal kernels ---------------------------------------------------------
template <typename T, int matrix_width, int matrix_height>
void CPUKernel<T, matrix_width, matrix_height>::kernel2(T *p_real, T *p_imag) {
    for (int y = 0; y < matrix_height; y++) {
        for (int idx = y * matrix_width + y % 2, peer = idx + 1; idx < (y + 1) * matrix_width - 1; idx += 2, peer += 2) {
            T tmp_real = p_real[idx];
            T tmp_imag = p_imag[idx];
            p_real[idx] = a * tmp_real - b * p_imag[peer];
            p_imag[idx] = a * tmp_imag + b * p_real[peer];
            p_real[peer] = a * p_real[peer] - b * tmp_imag;
            p_imag[peer] = a * p_imag[peer] + b * tmp_real;
        }
    }
}

template <typename T, int matrix_width, int matrix_height>
void CPUKernel<T, matrix_width, matrix_height>::kernel4(T *p_real, T *p_imag) {
    for (int y = 0; y < matrix_height; y++) {
        for (int idx = y * matrix_width + 1 - (y % 2), peer = idx + 1; idx < (y + 1) * matrix_width - 1; idx += 2, peer += 2) {
            T tmp_real = p_real[idx];
            T tmp_imag = p_imag[idx];
            p_real[idx] = a * tmp_real - b * p_imag[peer];
            p_imag[idx] = a * tmp_imag + b * p_real[peer];
            p_real[peer] = a * p_real[peer] - b * tmp_imag;
            p_imag[peer] = a * p_imag[peer] + b * tmp_real;
        }
    }
}

#endif
