#ifndef __CPUBLOCK_H
#define __CPUBLOCK_H

#include <algorithm>
#include <cstring>

#ifdef _OPENMP
#include <omp.h>
#endif

#include "../common.h"
#include "../trotterkernel.h"

template <typename T, int matrix_width, int matrix_height>
class CPUBlock: public ITrotterKernel<T> {
public:
    CPUBlock(T *p_real, T *p_imag, T a, T b);
    ~CPUBlock();
    void run_kernels();
    void run_kernel(int k);
    void wait_for_completion();
    void copy_results();
    void get_sample(size_t dest_stride, size_t x, size_t y, size_t width, size_t height, T * dest_real, T * dest_imag) const;

    bool runs_in_place() const { return false; }
    std::string get_name() const {
#ifdef _OPENMP
        std::stringstream name;
        name << "OpenMP block kernel (" << omp_get_max_threads() << " threads)";
        return name.str();
#else
        return "CPU block kernel";
#endif
    };

private:
    void kernel8(const T *p_real, const T *p_imag, T * next_real, T * next_imag);

    T *orig_real;
    T *orig_imag;
    T *p_real[2];
    T *p_imag[2];
    T a;
    T b;
    int sense;

    static const size_t HALO_X = 4u; // NOTE: min is 3 but it messes up block offsets
    static const size_t HALO_Y = 4u;
    static const size_t BLOCK_WIDTH = 128u;
    static const size_t BLOCK_HEIGHT = 128u;
};


// Helpers
template <typename T>
static void block_kernel_vertical(size_t start_offset, size_t stride, size_t width, size_t height, T a, T b, T * p_real, T * p_imag) {
    for (size_t idx = start_offset, peer = idx + stride; idx < width; idx += 2, peer += 2) {
        T tmp_real = p_real[idx];
        T tmp_imag = p_imag[idx];
        p_real[idx] = a * tmp_real - b * p_imag[peer];
        p_imag[idx] = a * tmp_imag + b * p_real[peer];
        p_real[peer] = a * p_real[peer] - b * tmp_imag;
        p_imag[peer] = a * p_imag[peer] + b * tmp_real;
    }
    for (size_t y = 1; y < height - 1; ++y) {
        for (size_t idx = y * stride + (start_offset + y) % 2, peer = idx + stride; idx < y * stride + width; idx += 2, peer += 2) {
            T tmp_real = p_real[idx];
            T tmp_imag = p_imag[idx];
            p_real[idx] = a * tmp_real - b * p_imag[peer];
            p_imag[idx] = a * tmp_imag + b * p_real[peer];
            p_real[peer] = a * p_real[peer] - b * tmp_imag;
            p_imag[peer] = a * p_imag[peer] + b * tmp_real;
        }
    }
}


template <typename T>
static void block_kernel_horizontal(size_t start_offset, size_t stride, size_t width, size_t height, T a, T b, T * p_real, T * p_imag) {
    for (size_t y = 0; y < height; ++y) {
        for (size_t idx = y * stride + (start_offset + y) % 2, peer = idx + 1; idx < y * stride + width - 1; idx += 2, peer += 2) {
            T tmp_real = p_real[idx];
            T tmp_imag = p_imag[idx];
            p_real[idx] = a * tmp_real - b * p_imag[peer];
            p_imag[idx] = a * tmp_imag + b * p_real[peer];
            p_real[peer] = a * p_real[peer] - b * tmp_imag;
            p_imag[peer] = a * p_imag[peer] + b * tmp_real;
        }
    }
}


template <typename T>
static void full_step(size_t stride, size_t width, size_t height, float a, float b, float * real, float * imag) {
    block_kernel_vertical<T>  (0u, stride, width, height, a, b, real, imag);
    block_kernel_horizontal<T>(0u, stride, width, height, a, b, real, imag);
    block_kernel_vertical<T>  (1u, stride, width, height, a, b, real, imag);
    block_kernel_horizontal<T>(1u, stride, width, height, a, b, real, imag);
    block_kernel_horizontal<T>(1u, stride, width, height, a, b, real, imag);
    block_kernel_vertical<T>  (1u, stride, width, height, a, b, real, imag);
    block_kernel_horizontal<T>(0u, stride, width, height, a, b, real, imag);
    block_kernel_vertical<T>  (0u, stride, width, height, a, b, real, imag);
}


template <typename T>
static void full_step(size_t stride, size_t width, size_t height, T a, T b, T * real, T * imag) {
    block_kernel_vertical  (0u, stride, width, height, a, b, real, imag);
    block_kernel_horizontal(0u, stride, width, height, a, b, real, imag);
    block_kernel_vertical  (1u, stride, width, height, a, b, real, imag);
    block_kernel_horizontal(1u, stride, width, height, a, b, real, imag);
    block_kernel_horizontal(1u, stride, width, height, a, b, real, imag);
    block_kernel_vertical  (1u, stride, width, height, a, b, real, imag);
    block_kernel_horizontal(0u, stride, width, height, a, b, real, imag);
    block_kernel_vertical  (0u, stride, width, height, a, b, real, imag);
}


template <typename T, size_t halo_x, size_t block_width, size_t block_height, size_t matrix_width, size_t matrix_height>
static void process_band(size_t read_y, size_t read_height, size_t write_offset, size_t write_height, T a, T b, const T * p_real, const T * p_imag, T * next_real, T * next_imag) {
    T block_real[block_height * block_width];
    T block_imag[block_height * block_width];

    if (matrix_width <= block_width) {
        // One full block
        memcpy2D(block_real, block_width * sizeof(T), &p_real[read_y * matrix_width], matrix_width * sizeof(T), matrix_width * sizeof(T), read_height);
        memcpy2D(block_imag, block_width * sizeof(T), &p_imag[read_y * matrix_width], matrix_width * sizeof(T), matrix_width * sizeof(T), read_height);
        full_step<T>(block_width, matrix_width, read_height, a, b, block_real, block_imag);
        memcpy2D(&next_real[(read_y + write_offset) * matrix_width], matrix_width * sizeof(T), &block_real[write_offset * block_width], block_width * sizeof(T), matrix_width * sizeof(T), write_height);
        memcpy2D(&next_imag[(read_y + write_offset) * matrix_width], matrix_width * sizeof(T), &block_imag[write_offset * block_width], block_width * sizeof(T), matrix_width * sizeof(T), write_height);
    } else {
        // First block [0..block_width - halo_x]
        memcpy2D(block_real, block_width * sizeof(T), &p_real[read_y * matrix_width], matrix_width * sizeof(T), block_width * sizeof(T), read_height);
        memcpy2D(block_imag, block_width * sizeof(T), &p_imag[read_y * matrix_width], matrix_width * sizeof(T), block_width * sizeof(T), read_height);
        full_step<T>(block_width, block_width, read_height, a, b, block_real, block_imag);
        memcpy2D(&next_real[(read_y + write_offset) * matrix_width], matrix_width * sizeof(T), &block_real[write_offset * block_width], block_width * sizeof(T), (block_width - halo_x) * sizeof(T), write_height);
        memcpy2D(&next_imag[(read_y + write_offset) * matrix_width], matrix_width * sizeof(T), &block_imag[write_offset * block_width], block_width * sizeof(T), (block_width - halo_x) * sizeof(T), write_height);

        size_t block_start;
        for (block_start = block_width - 2 * halo_x; block_start < matrix_width - block_width; block_start += block_width - 2 * halo_x) {
            memcpy2D(block_real, block_width * sizeof(T), &p_real[read_y * matrix_width + block_start], matrix_width * sizeof(T), block_width * sizeof(T), read_height);
            memcpy2D(block_imag, block_width * sizeof(T), &p_imag[read_y * matrix_width + block_start], matrix_width * sizeof(T), block_width * sizeof(T), read_height);
            full_step<T>(block_width, block_width, read_height, a, b, block_real, block_imag);
            memcpy2D(&next_real[(read_y + write_offset) * matrix_width + block_start + halo_x], matrix_width * sizeof(T), &block_real[write_offset * block_width + halo_x], block_width * sizeof(T), (block_width - 2 * halo_x) * sizeof(T), write_height);
            memcpy2D(&next_imag[(read_y + write_offset) * matrix_width + block_start + halo_x], matrix_width * sizeof(T), &block_imag[write_offset * block_width + halo_x], block_width * sizeof(T), (block_width - 2 * halo_x) * sizeof(T), write_height);
        }
        // Last block
        memcpy2D(block_real, block_width * sizeof(T), &p_real[read_y * matrix_width + block_start], matrix_width * sizeof(T), (matrix_width - block_start) * sizeof(T), read_height);
        memcpy2D(block_imag, block_width * sizeof(T), &p_imag[read_y * matrix_width + block_start], matrix_width * sizeof(T), (matrix_width - block_start) * sizeof(T), read_height);
        full_step<T>(block_width, matrix_width - block_start, read_height, a, b, block_real, block_imag);
        memcpy2D(&next_real[(read_y + write_offset) * matrix_width + block_start + halo_x], matrix_width * sizeof(T), &block_real[write_offset * block_width + halo_x], block_width * sizeof(T), (matrix_width - block_start - halo_x) * sizeof(T), write_height);
        memcpy2D(&next_imag[(read_y + write_offset) * matrix_width + block_start + halo_x], matrix_width * sizeof(T), &block_imag[write_offset * block_width + halo_x], block_width * sizeof(T), (matrix_width - block_start - halo_x) * sizeof(T), write_height);
    }
}


// Class methods
template <typename T, int matrix_width, int matrix_height>
CPUBlock<T, matrix_width, matrix_height>::CPUBlock(T *_p_real, T *_p_imag, T _a, T _b):
    orig_real(_p_real),
    orig_imag(_p_imag),
    a(_a),
    b(_b),
    sense(0)
{
    p_real[0] = new T[matrix_width * matrix_height];
    p_real[1] = new T[matrix_width * matrix_height];
    p_imag[0] = new T[matrix_width * matrix_height];
    p_imag[1] = new T[matrix_width * matrix_height];

    std::copy(_p_real, _p_real + matrix_width * matrix_height, p_real[0]);
    std::copy(_p_imag, _p_imag + matrix_width * matrix_height, p_imag[0]);
}

template <typename T, int matrix_width, int matrix_height>
CPUBlock<T, matrix_width, matrix_height>::~CPUBlock() {
    delete[] p_real[0];
    delete[] p_real[1];
    delete[] p_imag[0];
    delete[] p_imag[1];
}

template <typename T, int matrix_width, int matrix_height>
void CPUBlock<T, matrix_width, matrix_height>::run_kernel(int k) {
    switch (k) {
    case 1:
        kernel8(p_real[sense], p_imag[sense], p_real[1-sense], p_imag[1-sense]);
        break;

    default:
        // TODO: throw
        break;
    }
    sense = 1 - sense;
}

template <typename T, int matrix_width, int matrix_height>
void CPUBlock<T, matrix_width, matrix_height>::run_kernels() {
    run_kernel(1);
}

template <typename T, int matrix_width, int matrix_height>
void CPUBlock<T, matrix_width, matrix_height>::wait_for_completion() { }

template <typename T, int matrix_width, int matrix_height>
void CPUBlock<T, matrix_width, matrix_height>::copy_results() {
    std::copy(p_real[sense], p_real[sense] + matrix_width * matrix_height, orig_real);
    std::copy(p_imag[sense], p_imag[sense] + matrix_width * matrix_height, orig_imag);
}


template <typename T, int matrix_width, int matrix_height>
void CPUBlock<T, matrix_width,matrix_height>::get_sample(size_t dest_stride, size_t x, size_t y, size_t width, size_t height, T * dest_real, T * dest_imag) const {
    memcpy2D(dest_real, dest_stride * sizeof(T), &(p_real[sense][y * matrix_width + x]), matrix_width * sizeof(T), width * sizeof(T), height);
    memcpy2D(dest_imag, dest_stride * sizeof(T), &(p_imag[sense][y * matrix_width + x]), matrix_width * sizeof(T), width * sizeof(T), height);
}


template <typename T, int matrix_width, int matrix_height>
void CPUBlock<T, matrix_width, matrix_height>::kernel8(const T *p_real, const T *p_imag, T * next_real, T * next_imag) {

    if (matrix_height <= BLOCK_HEIGHT) {
        // One full band
        process_band<T, HALO_X, BLOCK_WIDTH, BLOCK_HEIGHT, matrix_width, matrix_height>(0, matrix_height, 0, matrix_height, a, b, p_real, p_imag, next_real, next_imag);
    } else {

#ifdef _OPENMP
        size_t block_start;
        #pragma omp parallel default(shared) private(block_start)
        {
            #pragma omp sections nowait
            {
                #pragma omp section
                {
                    // First band
                    process_band<T, HALO_X, BLOCK_WIDTH, BLOCK_HEIGHT, matrix_width, matrix_height>(0, BLOCK_HEIGHT, 0, BLOCK_HEIGHT - HALO_Y, a, b, p_real, p_imag, next_real, next_imag);
                }
                #pragma omp section
                {
                    // Last band
                    block_start = matrix_height - BLOCK_HEIGHT + (matrix_height - BLOCK_HEIGHT) % (BLOCK_HEIGHT - 2 * HALO_Y);
                    process_band<T, HALO_X, BLOCK_WIDTH, BLOCK_HEIGHT, matrix_width, matrix_height>(block_start, matrix_height - block_start, HALO_Y, matrix_height - block_start - HALO_Y, a, b, p_real, p_imag, next_real, next_imag);
                }
            }

            #pragma omp for schedule(runtime) nowait
            for (block_start = BLOCK_HEIGHT - 2 * HALO_Y; block_start < matrix_height - BLOCK_HEIGHT; block_start += BLOCK_HEIGHT - 2 * HALO_Y) {
                process_band<T, HALO_X, BLOCK_WIDTH, BLOCK_HEIGHT, matrix_width, matrix_height>(block_start, BLOCK_HEIGHT, HALO_Y, BLOCK_HEIGHT - 2 * HALO_Y, a, b, p_real, p_imag, next_real, next_imag);
            }

            #pragma omp barrier
        }
#else
        // First band
        process_band<T, HALO_X, BLOCK_WIDTH, BLOCK_HEIGHT, matrix_width, matrix_height>(0, BLOCK_HEIGHT, 0, BLOCK_HEIGHT - HALO_Y, a, b, p_real, p_imag, next_real, next_imag);

        size_t block_start;
        for (block_start = BLOCK_HEIGHT - 2 * HALO_Y; block_start < matrix_height - BLOCK_HEIGHT; block_start += BLOCK_HEIGHT - 2 * HALO_Y) {
            process_band<T, HALO_X, BLOCK_WIDTH, BLOCK_HEIGHT, matrix_width, matrix_height>(block_start, BLOCK_HEIGHT, HALO_Y, BLOCK_HEIGHT - 2 * HALO_Y, a, b, p_real, p_imag, next_real, next_imag);
        }

        // Last band
        process_band<T, HALO_X, BLOCK_WIDTH, BLOCK_HEIGHT, matrix_width, matrix_height>(block_start, matrix_height - block_start, HALO_Y, matrix_height - block_start - HALO_Y, a, b, p_real, p_imag, next_real, next_imag);
#endif /* _OPENMP */
    }
}

#endif
